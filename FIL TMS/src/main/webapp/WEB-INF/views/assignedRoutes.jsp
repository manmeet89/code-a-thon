<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Route Management</title>
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>

<script type="text/javascript">
    $(document).ready(function () {
    	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    	$("input").jqxInput({height: 25, minLength: 1});
        $(".dropTime").jqxDropDownList({autoDropDownHeight: true, width: 150, height: 25 });
    	
    	var assigned = false;
    	
    	 //$(".dropTime").jqxDropDownList({ width: 300, height: 25 });
         $(".dropTime").jqxDropDownList({autoDropDownHeight: true, width: 190, height: 25 });
         $("input[type='button']").jqxButton({ width: 190});
    	$('.getAssignedRoutes, .getUnAssignedRoutes').click(function() {
    		var date  = $('.routeDate').val();
    		var timeSlot  = $('.dropTime').val();
    		var $url;
    		if($(this).hasClass('getAssignedRoutes') == true){
    			assigned = true;
    			$url = '${pageContext.request.contextPath}/getCabAssignedRoutes/' + date + '/' + timeSlot;
    		} else {
    			assigned = false;
    			$url = '${pageContext.request.contextPath}/getCabUnAssignedRoutes/' + date + '/' + timeSlot;
    		}
    		reloadRoutesList(date, timeSlot, $url);
    	});
    	
    	function reloadRoutesList(date, timeSlot, $url) {
    		
    		var slotsSource =
    		{
    			datatype: "json",
    			datafields: [
    				{ name: 'id',type: 'string'},
                    { name: 'name',type: 'string'}
    			],
    			url: '${pageContext.request.contextPath}/getSlots',
    		};
    		 var slotsAdapter = new $.jqx.dataAdapter(slotsSource);
    		
        	var source = {
                    datatype: 'json',
                    datafields: [{
                        name: 'routeId',
                        type: 'string'
                    }, {
                        name: 'transporter',
                        type: 'string'
                    }, {
                        name: 'routeNo',
                        type: 'string'
                    }, {
                        name: 'slotNo',
                        type: 'string'
                    }, {
                        name: 'vehicleNo',
                        type: 'string'
                    }],
                    id: 'routeId',
                    url: $url,
                    type: 'GET',
                    async: true,
                    updaterow: function (rowid, rowdata, commit) {
                    	commit(true);
                    }
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#jqxgrid').jqxGrid({
                    theme: 'arctic',
                    autoheight: true,
                    columnsmenu :false,
                    source: dataAdapter,
                    pageable: true,
                    sortable: true,
                    filterable:true,
                    editable: true,
                    showstatusbar: true,
                    renderstatusbar: function (statusbar) {
                    	var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                        // appends buttons to the status bar.
                        var saveChanges = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                        container.append(saveChanges);
                        statusbar.append(container);
                        saveChanges.jqxButton({  width: 65, height: 20 });
                        // save changes.
                        saveChanges.click(function() {
            	        	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
            	        	var rows = [];
            	        	for (var i = 0; i < griddata.rowscount; i++)
            	        	{
            		        	var myObj = new Object();
            		        	myObj.routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['routeId'];
            		        	myObj.vehicleNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vehicleNo'];
            		        	myObj.slotNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['slotNo'];
            		        	rows.push(myObj);
            	        	}
            	        	
            	        	$.ajax({
            	        		  type: "POST",
            	        		  url: "${pageContext.request.contextPath}/assignSlotOrVehicle",
            	        		  contentType: "application/json",
            	        		  data: JSON.stringify(rows),
            	        		  success:function(data) {
            	        			  if(assigned == true) {
            	        			  $('.getAssignedRoutes').trigger('click');
            	        			  } else {
            	        				  $('.getUnAssignedRoutes').trigger('click');
            	        			  }
            	        		    }
            	        		  });
            	        });
                    },
                    columns: [{
                        text: 'Route No',
                        datafield: 'routeNo',
                        editable: false,
                        align: 'center',
                        cellsalign: 'center'
                    },{
                        text: 'Transporter',
                        datafield: 'transporter',
                        align: 'center',
                        cellsalign: 'center',
                        editable: false,
                    }, {
                        text: 'Cab No.',
                        datafield: 'vehicleNo',
                        align: 'center',
                        cellsalign: 'center'
                        
                    },  
                    /* {
                        text: 'Slot No.',
                        datafield: 'slotNo',
                        align: 'center',
                        cellsalign: 'center'
                        
                    } */
                    {
                    	text: 'Slot No', datafield: 'slotNo', displayfield: 'slotNo', columntype: 'dropdownlist',
                    	createeditor: function (row, value, editor) {
                          editor.jqxDropDownList({ source: slotsAdapter, displayMember: 'name', valueMember: 'name', selectedIndex: 0 });
                      }
                	}
                    , {
                        datafield: 'routeId',
                        cellsformat: 'd', 
                        align: 'center',
                        cellsalign: 'center',
                        hidden : true
                    }]
                });
        }
    	
    	$(".routeDate").datepicker().datepicker("setDate", new Date());
    });
</script>
<style type="text/css">

td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
</style>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid" style="text-align: center;">
<div>
	<div>
		<table class = 'searchCriteria'>
		<tr>
			<td>Route Date</td>
			<td><input type="text" id="datepicker" class = 'routeDate' style="position: relative; z-index: 100000;"></td>
			<td>Select Time Slot</td>
			<td><select class = "dropTime">
				<option value="05:30 PM">05:30 PM</option>
				<option value="07:30 PM">07:30 PM</option>
			</select></td>
			<td>
			<input type="button" class="getAssignedRoutes" value="Get Assigned Routes" style="height:30px; width:auto"/></td>
			<td>
			<input type="button" class="getUnAssignedRoutes" value="Get Un-Assigned Routes" style="height:30px; width:auto"/></td>
		</tr>
		</table>
	</div>
	<p></p>
	<p></p>
	<div id="jqxgrid">
	
	</div>
	</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>
	
</body>
</html>