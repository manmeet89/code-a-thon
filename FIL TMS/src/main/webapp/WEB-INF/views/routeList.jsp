<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Route List</title>
    <style type="text/css">
    
    .ontop {
				z-index: 999;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				display: none;
				position: absolute;				
				background-color: #cccccc;
				color: #aaaaaa;
				opacity: .96;
				filter: alpha(opacity = 90);
			}
			#popup {
				width: 500px;
				height: 200px;
				position: absolute;
				color: black;
				background-color: white;
				/* To align popup window at the center of screen*/
				top: 50%;
				left: 50%;
				margin-top: -100px;
				margin-left: -150px;
			}
    </style>
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript">
        $(document).ready(function () {
        	
        	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
        	$("input").jqxInput({height: 25, minLength: 1});
            $(".dropDown").jqxDropDownList({autoDropDownHeight: true, width: 150, height: 25 });
            $("input[type='button']").jqxButton({ width: 'auto'});
            function reloadGrid() 
        	{
            	$('#routeListDiv').show();
            	var form = new Object();
            	form.date = $('.routeDate').val();
            	form.dropTime = $('.dropTime').val();
            	form.vendor = $('.vendorSelect').val();
            var source = {
                datatype: 'json',
                datafields: [{
                    name: 'htCode',
                    type: 'string'
                }, {
                    name: 'routeNo',
                    type: 'string'
                }, {
                    name: 'cType',
                    type: 'string'
                }, {
                    name: 'transporter',
                    type: 'string'
                }, {
                    name: 'vehicleNo',
                    type: 'string'
                }, {
                    name: 'area',
                    type: 'string'
                }, {
                    name: 'officeLocation',
                    type: 'string'
                }, {
                    name: 'multiplicityFactor',
                    type: 'string'
                }, {
                    name: 'startTime',
                    type: 'string'
                }, {
                    name: 'routeId',
                    type: 'string'
                }, {
                    name: 'routeDate',
                    type: 'string'
                }],
                id: 'routeId',
                url: '${pageContext.request.contextPath}/searchRoutes',
                data : {
                	date : $('.routeDate').val(),
                	dropTime : $('.dropTime').val(), 
                	vendor : $('.vendorSelect').val()
                	
                },
                type: 'POST',
                async: true,
                updaterow: function (rowid, rowdata, commit) {
                	
                	commit(true);
                }
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#jqxgrid').jqxGrid({
                theme: 'arctic',
                width : '100%',
                autoheight: true,
                source: dataAdapter,
                pageable: true,
                sortable: true,
                filterable:false,
                columnsmenu :false,
                editable: true,
                showstatusbar: true,
                renderstatusbar: function (statusbar) {
                    // appends buttons to the status bar.
                    var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                    var saveChanges = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                    container.append(saveChanges);
                    statusbar.append(container);
                    saveChanges.jqxButton({  width: 65, height: 20 });
                    // save changes.
                    saveChanges.click(function() {
        	        	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
        	        	var rows = [];
        	        	for (var i = 0; i < griddata.rowscount; i++)
        	        	{
        		        	var myObj = new Object();
        		        	myObj.routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['routeId'];
        		        	myObj.htCode = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['htCode'];
        		        	myObj.routeNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['routeNo'];
        		        	myObj.cType = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['cType'];
        		        	myObj.transporter = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['transporter'];
        		        	myObj.vehicleNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vehicleNo'];
        		        	myObj.area = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['area'];
        		        	
        		        	myObj.officeLocation = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['officeLocation'];
        		        	myObj.multiplicityFactor = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['multiplicityFactor'];
        		        	myObj.startTime = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['startTime'];
        		        	myObj.routeDate = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['routeDate'];
        		        	
        		        	rows.push(myObj);
        	        	}
        	        	console.log(JSON.stringify(rows));
        	        	
        	        	$.ajax({
        	        		  type: "POST",
        	        		  url: "${pageContext.request.contextPath}/updateRoutes",
        	        		  contentType: "application/json",
        	        		  data: JSON.stringify(rows),
        	        		  success:function(data) {
        	        			alert('success');
        	        		    }
        	        		  });
        	        });
                },
                columns: [{
                    text: 'HT Code',
                    datafield: 'htCode',
                    width: '10%', 
                    align: 'center',
                    cellsalign: 'center',
                    editable: false,
                    children: {source: dataAdapter}
                }, {
                    text: 'Route No',
                    datafield: 'routeNo',
                    width: '10%',
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Carrier Type',
                    datafield: 'cType',
                    width: '10%', 
                    align: 'center',
                    editable: false,
                    cellsalign: 'center'
                }, {
                    text: 'Vendor',
                    datafield: 'transporter',
                    align: 'center',
                    cellsalign: 'center',
                    width: '10%'
                }, {
                    text: 'Vehicle No',
                    datafield: 'vehicleNo',
                    width: '10%', 
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Area',
                    datafield: 'area',
                    width: '10%', 
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Office Location',
                    datafield: 'officeLocation',
                    width: '10%', 
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Multiplicity Factor',
                    datafield: 'multiplicityFactor',
                    cellsformat: 'd', 
                    align: 'center',
                    cellsalign: 'center',
                    editable: false,
                    width: '10%'
                }, {
                    text: 'Start Time',
                    datafield: 'startTime',
                    cellsformat: 'd', 
                    align: 'center',
                    cellsalign: 'center',
                    editable: false,
                    width: '10%'
                }, {
                    text: 'Date',
                    datafield: 'routeDate',
                    cellsformat: 'd', 
                    align: 'center',
                    cellsalign: 'center',
                    editable: false,
                    width: '10%'
                }, {
                    datafield: 'routeId',
                    cellsformat: 'd', 
                    align: 'center',
                    cellsalign: 'center',
                    hidden : true
                }]
            });
        	}
            
            $("#jqxgrid").bind('rowselect', function (event) {
            	var selectedrowindex = event.args.rowindex;
            	var routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
            	reloadPassengersList(routeId);
            });
            
            function reloadPassengersList(routeId) {
            	$('#employeeListDiv').show();
            	var source = {
                        datatype: 'json',
                        datafields: [{
                            name: 'passengerId',
                            type: 'string'
                        }, {
                            name: 'serialNo',
                            type: 'string'
                        }, {
                            name: 'corpId',
                            type: 'string'
                        }, {
                            name: 'routeId',
                            type: 'string'
                        }, {
                            name: 'name',
                            type: 'string'
                        }, {
                            name: 'address',
                            type: 'string'
                        }, {
                            name: 'pickupPoint',
                            type: 'string'
                        }, {
                            name: 'mobileNo',
                            type: 'string'
                        }],
                        id: 'passengerId',
                        url: '${pageContext.request.contextPath}/getPassengersForRoute',
                        data : {routeId : routeId},
                        type: 'POST',
                        async: true,
                        updaterow: function (rowid, rowdata, commit) {
                        	
                        	commit(true);
                        }
                    };
                    var dataAdapter = new $.jqx.dataAdapter(source);
                    $('#jqxgridEmployee').jqxGrid({
                        theme: 'arctic',
                        autoheight: true,
                        source: dataAdapter,
                        pageable: true,
                        sortable: true,
                        filterable:true,
                        columnsmenu :false,
                        editable: true,
                        width : 1000,
                        showstatusbar: true,
                        renderstatusbar: function (statusbar) {
                            // appends buttons to the status bar.
                            var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                            var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                            var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                            var saveChanges = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                            container.append(addButton);
                            container.append(deleteButton);
                            container.append(saveChanges);
                            statusbar.append(container);
                            addButton.jqxButton({  width: 60, height: 20 });
                            deleteButton.jqxButton({  width: 65, height: 20 });
                            saveChanges.jqxButton({  width: 65, height: 20 });
                            // add new row.
                            addButton.click(function (event) {
                          			  pop('popDiv');
                            });
                            // delete selected row.
                            deleteButton.click(function (event) {
                                var selectedrowindex = $("#jqxgridEmployee").jqxGrid('getselectedrowindex');
                                var $passengerId = $("#jqxgridEmployee").jqxGrid('getrowid', selectedrowindex);
                                $("#jqxgridEmployee").jqxGrid('deleterow', $passengerId);
                                
                                $.ajax({
              	        		  type: "POST",
              	        		  url: "${pageContext.request.contextPath}/removePassengerFromRoute",
              	        		  data: {
              	        			passengerId : $passengerId
              	        		  },
              	        		  success:function(data) {
              	        			  //hide('popDiv');
              	        			  //window.location.reload();
              	        			  //alert('Employee removed successfully');
              	        		    }
              	        		  });
                            });
                            // save changes.
                            saveChanges.click(function() {
                            	var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                            	var routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
                            	
                	        	var griddata = $('#jqxgridEmployee').jqxGrid('getdatainformation');
                	        	var rows = [];
                	        	for (var i = 0; i < griddata.rowscount; i++)
                	        	{
                		        	var myObj = new Object();
                		        	myObj.routeId = routeId;
                		        	myObj.serialNo = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['serialNo'];
                		        	myObj.corpId = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['corpId'];
                		        	myObj.name = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['name'];
                		        	myObj.passengerId = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['passengerId'];
                		        	myObj.address = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['address'];
                		        	myObj.pickupPoint = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['pickupPoint'];
                		        	myObj.mobileNo = $('#jqxgridEmployee').jqxGrid('getrenderedrowdata', i)['mobileNo'];
                		        	rows.push(myObj);
                	        	}
                	        	console.log(JSON.stringify(rows));
                	        	
                	        	$.ajax({
                	        		  type: "POST",
                	        		  url: "${pageContext.request.contextPath}/updateRoutePassengers",
                	        		  contentType: "application/json",
                	        		  data: JSON.stringify(rows),
                	        		  success:function(data) {
                	        			var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                	                  	var routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
                	        			  reloadPassengersList(routeId);
                	        		    }
                	        		  });
                	        });
                        },
                        columns: [{
                            text: 'S.No',
                            datafield: 'serialNo',
                            align: 'center',
                            cellsalign: 'center',
                            width : '10%'
                        },{
                            text: 'Corp Id',
                            datafield: 'corpId',
                            align: 'center',
                            cellsalign: 'center',
                            editable: false,
                            width : '10%'
                        }, {
                            text: 'Name',
                            datafield: 'name',
                            align: 'center',
                            cellsalign: 'center',
                            width : '20%'
                            
                        },  {
                            text: 'Address',
                            datafield: 'address',
                            align: 'center',
                            cellsalign: 'center',
                            width : '20%'
                        }, {
                            text: 'Pick Up Point',
                            datafield: 'pickupPoint',
                            align: 'center',
                            cellsalign: 'center'
                        } , {
                            text: 'Mobile No.',
                            datafield: 'mobileNo',
                            align: 'center',
                            cellsalign: 'center',
                            width : '20%'
                        }, {
                            datafield: 'routeId',
                            cellsformat: 'd', 
                            align: 'center',
                            cellsalign: 'center',
                            hidden : true
                        }, {
                            datafield: 'passengerId',
                            cellsformat: 'd', 
                            align: 'center',
                            cellsalign: 'center',
                            hidden : true
                        }]
                    });
            }
            
            function pop(div) {
				document.getElementById(div).style.display = 'block';
			}
			function hide(div) {
				document.getElementById(div).style.display = 'none';
			}
			
			document.onkeydown = function(evt) {
				evt = evt || window.event;
				if (evt.keyCode == 27) {
					hide('popDiv');
				}
			};
			
			$('.addPassengerBtnConfirm').click(function() {
				
				var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
            	var routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
				
				var myObj = new Object();
	        	myObj.routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
	        	myObj.serialNo = $('.sNo').val();
	        	myObj.corpId = $('.corpId').val();
	        	myObj.name = $('.empName').val();
	        	myObj.address = $('.empAddress').val();
	        	myObj.pickupPoint = $('.pickUpPoint').val();
	        	
	        	$.ajax({
	        		  type: "POST",
	        		  url: "${pageContext.request.contextPath}/addPassengerToRoute",
	        		  contentType: "application/json",
	        		  data: JSON.stringify(myObj),
	        		  success:function(data) {
	        			  hide('popDiv');
	        			  reloadPassengersList(routeId);
	        		    }
	        		  });
	        });
			
			$('.searchBtn').click(function() {
				if($('.routeDate').val() == ''){
					alert('Please enter date !!');
					return;
				}
				reloadGrid();
				$('#employeeListDiv').hide();
			});
			
			
			
			$(".routeDate").datepicker().datepicker("setDate", new Date());
			$('.searchBtn').trigger('click');
			
			$('.closeButton').click(function() {
	        	hide('popDiv');
	        });
			
        });
        
    </script>
    
    <style type="text/css">
   
   td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
   
   
   </style>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">

<div class="searchCriteria">
<table class = 'searchCriteria'>
		<tr>
			<td>Route Date</td>
			<td><input type="text" id="datepicker" class = 'routeDate' style="position: relative; z-index: 100000;"></td>
			<td>Select Time Slot</td>
			<td><select class = "dropDown dropTime">
				<option value="ALL">ALL</option>
				<option value="05:30 PM">05:30 PM</option>
				<option value="07:30 PM">07:30 PM</option>
			</select></td>
			<td>Select Vendor</td>
			<td><select class = " dropDown vendorSelect">
			<option value="ALL">ALL</option>
			<c:forEach items="${vendors}" var="vendor">
				<option value="${vendor.name}">${vendor.name}</option>
			</c:forEach>
			</select></td>
			<td colspan="2">
			<input type="button" class="searchBtn" value="Search" style="height:30px; width:100px"/></td>
		</tr>
		</table>
</div>
<div>
	<div id="routeListDiv" style="display: none">
		<p style="font-weight: bold;">Route list</p>
		<div id="jqxgrid"></div>
	</div>

	<div id="employeeListDiv" style="display: none">
				<p style="font-weight: bold;">Employee list</p>
		<div id="jqxgridEmployee"></div>
	</div>
	
	
	<!-- Start -->
	<div id="popDiv" class="ontop" style="display: none">
			<table border="1" id="popup" style="font-size: large;">
			<tr><td colspan="2" style="font-size: x-large; font-weight: bold;">Add passenger form
			<div class="closeButton" style="float: right;"><img style="height: 20px; width: 20px;"
				src="resources/images/close_my_icon.jpg"></div>
			
			</td></tr>
			<tr>
			<td>S No.</td>
			<td><input type="text" class="sNo"></td>
			</tr>
			<tr>
			<tr>
			<td>A Number</td>
			<td><input type="text" class="corpId"></td>
			</tr>
			<tr>
			<td>Name</td>
			<td><input type="text" class="empName"></td>
			</tr>
			<tr>
			<td>Address</td>
			<td><input type="text" class="empAddress"></td>
			</tr>
			<tr>
			
			<tr>
			<td>Pick Up Point</td>
			<td><input type="text" class="pickUpPoint"></td>
			</tr>
			<tr>
			<td colspan="2" align="center"><input type="button" class="addPassengerBtnConfirm" value="Add" style="height:30px; width:100px"/></td>
			</tr>
			</table>
		</div>
	<!-- Ends -->
	</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>
</body>
</html>