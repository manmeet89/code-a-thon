<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>FIL Transport system</title>
    
    <style type="text/css">
    
    .ontop {
				z-index: 999;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				display: none;
				position: absolute;				
				background-color: #cccccc;
				color: #aaaaaa;
				opacity: .96;
				filter: alpha(opacity = 90);
			}
			#popup {
				width: 500px;
				height: 200px;
				position: absolute;
				color: black;
				background-color: white;
				/* To align popup window at the center of screen*/
				top: 50%;
				left: 50%;
				margin-top: -100px;
				margin-left: -150px;
			}
    </style>
    
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        	
        	$("input").jqxInput({placeHolder: "Enter...", height: 25, width: 200, minLength: 1});
        	$("input[type='button']").jqxButton({ width: '150'});
        	function reloadGrid() 
        	{
        	var source = {
                datatype: 'json',
                datafields: [{
                    name: 'id',
                    type: 'string'
                }, {
                    name: 'vendorName',
                    type: 'string'
                }, {
                    name: 'regNumber',
                    type: 'string'
                }, {
                    name: 'model',
                    type: 'string'
                }],
                id: 'id',
                url: '${pageContext.request.contextPath}/getVehicles',
                type: 'POST',
                async: true,
                updaterow: function (rowid, rowdata, commit) {
                	
                	commit(true);
                }
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#jqxgrid').jqxGrid({
                theme: 'arctic',
                width : 600,
                autoheight: true,
                source: dataAdapter,
                pageable: true,
                sortable: true,
                columnsmenu :false,
                filterable:true,
                editable: true,
                showstatusbar: true,
                renderstatusbar: function (statusbar) {
                    // appends buttons to the status bar.
                    var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                    var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                    var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                    var reloadButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                    container.append(addButton);
                    container.append(deleteButton);
                    container.append(reloadButton);
                    statusbar.append(container);
                    addButton.jqxButton({  width: 60, height: 20 });
                    deleteButton.jqxButton({  width: 65, height: 20 });
                    reloadButton.jqxButton({  width: 65, height: 20 });
                    // add new row.
                    addButton.click(function (event) {
                  			  pop('popDiv');
                    });
                    // delete selected row.
                    deleteButton.click(function (event) {
                        var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                        var $vehicleId = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                        $("#jqxgrid").jqxGrid('deleterow', $vehicleId);
                        $.ajax({
      	        		  type: "POST",
      	        		  url: "${pageContext.request.contextPath}/deleteVehicle",
      	        		  data: {
      	        			  vehicleId : $vehicleId
      	        		  },
      	        		  success:function(data) {
      	        			  hide('popDiv');
      	        			  reloadGrid();
      	        		    }
      	        		  });
                    });
                    // reload grid data.
                    reloadButton.click(function (event) {
                    	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
        	        	var rows = [];
        	        	for (var i = 0; i < griddata.rowscount; i++)
        	        	{
        		        	var myObj = new Object();
        		        	myObj.id = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['id'];
        		        	myObj.vendorId = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vendorId'];
        		        	myObj.regNumber = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['regNumber'];
        		        	myObj.vendorName = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vendorName'];
        		        	myObj.model = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['model'];
        		        	rows.push(myObj);
        	        	}
        	        	console.log(JSON.stringify(rows));
        	        	
        	        	$.ajax({
        	        		  type: "POST",
        	        		  url: "${pageContext.request.contextPath}/updateVehicle",
        	        		  contentType: "application/json",
        	        		  data: JSON.stringify(rows),
        	        		  success:function(data) {
        	        			  window.location.reload();
        	        		    }
        	        		  });
        	        });
                },
                columns: [{
                    text: 'Vehicle Id',
                    datafield: 'id',
                    editable: false,
                    align: 'center',
                    cellsalign: 'center',
              	    hidden : true
                }, {
                    text: 'Vendor Name',
                    datafield: 'vendorName',
                    width: '30%',
                    editable: false,
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Registration Number',
                    datafield: 'regNumber',
                    width: '40%', 
                    align: 'center',
                    cellsalign: 'center'
                }, {
                    text: 'Model',
                    datafield: 'model',
                    align: 'center',
                    cellsalign: 'center',
                    width: '30%'
                }]
            });
        	}
        	reloadGrid();
            function displayButtons(cellvalue, options, rowObject)
            {
                var edit = "<input style='...' type='button' value='Edit' onclick=\"jQuery('#theGrid').editRow('" + options.rowId + "');\"  />", 
                     save = "<input style='...' type='button' value='Save' onclick=\"jQuery('#theGrid').saveRow('" + options.rowId + "');\"  />", 
                     restore = "<input style='...' type='button' value='Restore' onclick=\"jQuery('#theGrid').restoreRow('" + options.rowId + "');\" />";
                return edit+save+restore;
            }
            
            function pop(div) {
				document.getElementById(div).style.display = 'block';
			}
			function hide(div) {
				document.getElementById(div).style.display = 'none';
			}
            
            $('.addVehicleBtn').click(function() {
            	$.ajax({
            		  url: "${pageContext.request.contextPath}/addVehicle",
            		})
            		  .done(function( html ) {
            			  pop('popDiv');
            		  });
            });
            document.onkeydown = function(evt) {
				evt = evt || window.event;
				if (evt.keyCode == 27) {
					hide('popDiv');
				}
			};
			
			$('.addVehicleBtnConfirm').click(function() {
	        	$.ajax({
	        		  type: "POST",
	        		  url: "${pageContext.request.contextPath}/saveVehicle",
	        		  data: {
	        			  vendorId : $('.vendorSelect').val(),
	        			  regNumber : $('.regNumber').val(),
	        			  model : $('.model').val()
	        		  },
	        		  success:function(data) {
	        			  hide('popDiv');
	        			  reloadGrid();
	        		    }
	        		  });
	        });
	        
	        $('.refreshBtn').click(function() {
	        	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
	        	var rows = [];
	        	for (var i = 0; i < griddata.rowscount; i++)
	        	{
		        	var myObj = new Object();
		        	myObj.id = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['id'];
		        	myObj.vendorId = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vendorId'];
		        	myObj.regNumber = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['regNumber'];
		        	myObj.vendorName = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vendorName'];
		        	myObj.model = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['model'];
		        	rows.push(myObj);
	        	}
	        	console.log(JSON.stringify(rows));
	        	
	        	$.ajax({
	        		  type: "POST",
	        		  url: "${pageContext.request.contextPath}/updateVehicle",
	        		  contentType: "application/json",
	        		  data: JSON.stringify(rows),
	        		  success:function(data) {
	        			  reloadGrid();
	        		    }
	        		  });
	        });
	        
	        $('.closeButton').click(function() {
	        	hide('popDiv');
	        });
	        
	        $(".dropDown").jqxDropDownList({autoDropDownHeight: true, width: 150, height: 25 });
        });
    </script>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">
<div>

<p style="font-weight: bold;">Vehicles list</p>
    <div id="jqxgrid"></div>
    <br/>
    <!-- <div id="addVehicleDiv">
    	<input type="button" class="addVehicleBtn" value="Add Vehicle" style="height:30px; width:150px"/>
    	<input type="button" class="refreshBtn" value="Rfresh Data" style="height:30px; width:150px"/>
    </div> -->
     <div id="popDiv" class="ontop" style="display: none">
			<table border="1" id="popup" style="font-size: large;">
			<tr><td colspan="2" style="font-size: x-large; font-weight: bold;">Add vehicle form
			<div class="closeButton" style="float: right;"><img style="height: 20px; width: 20px;"
				src="resources/images/close_my_icon.jpg"></div>
			</td></tr>
			<tr>
			<td>Vendor</td>
			<td><select class = "vendorSelect dropDown">
			<c:forEach items="${vendors}" var="vendor">
				<option value="${vendor.id}">${vendor.name}</option>
			</c:forEach>
			</select></td>
			</tr>
			<tr>
			<td>Registration Number</td>
			<td><input type="text" class="regNumber"></td>
			</tr>
			<tr>
			<td>Model</td>
			<td><input type="text" class="model"></td>
			</tr>
			<tr>
			<td colspan="2" align="center"><input type="button" class="addVehicleBtnConfirm" value="Add" style="height:30px; width:100px"/></td>
			</tr>
			</table>
		</div>
		
		</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>
		
</body>
</html>