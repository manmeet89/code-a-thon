<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Upload Roaster</title>
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	
    	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    	$("button[type='submit'], input[name='file']").jqxButton({ width: 'auto'});
    	
    	$(".routeDate").datepicker().datepicker("setDate", new Date());
    });
   </script> 
   
   <style type="text/css">
   
   td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
   
   
   </style>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">

	<form method="POST" action="uploadRoaster" enctype="multipart/form-data">
	

<div>

<table>
<p style="font-weight: bold;">Upload Drop Details</p>
<tr>
<td>Roaster</td>
<td><input placeholder="Roaster File..." id="roasterFile" class= "searchTextBox" maxlength="200" tabindex="1"/>
<input id="exampleInputFile" type="file" name="file" ></td>
</tr>

<td>Date</td>
<td><input class= "searchTextBox routeDate" type="text" id="datepicker" name="routeDate" style="position: relative; z-index: 100000; width: 250px"></td>
</tr>

<tr><td colspan="2"><button type="submit"
							id="uploadFilesButton" 
							style="width: 150px; height: 35px;" value="Upload">Upload
							Roaster</button></tr>


</table>




<!-- <div style="" class="accountSeachDiv mainHeading">Upload Roaster</div>
<div style="margin-top:10px;">
<div style="display:inline;">
<input placeholder="Roaster File..." id="roasterFile" class= "searchTextBox" maxlength="200" tabindex="1"/>
<input id="exampleInputFile" type="file" name="file" >
</div>
</div>
<div style="margin-top:10px;"class="accountSeachDiv mainHeading">
<p>Date</p>
<input class= "searchTextBox" type="text" id="datepicker" name="routeDate" class = 'routeDate' style="position: relative; z-index: 100000; width: 250px">
</div>
<div class="mbr-buttons" style="margin-top: 50px;">
<button type="submit"
							id="uploadFilesButton" 
							style="width: 150px; height: 35px;" value="Upload">Upload
							Roaster</button>
</div> -->
	
	
	
	
		</form>
	</div>
	<p></p>
	<p></p>
	<div id="jqxgrid">
	
	</div>
	
	</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>

	
	
</body>
</html>