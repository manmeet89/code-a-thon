<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Cab Details</title>
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	
    	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    	$("button[type='submit'], input[name='file']").jqxButton({ width: 'auto'});
        $(".dropDown").jqxDropDownList({autoDropDownHeight: true, width: 200, height: 25 });
        
        $('#uploadFilesButton').click(function() {
        	if($('.routeDate').val() == '') {
        		alert('Please select Date first')
        		event.preventDefault();
        	}
        });
        
        $(".routeDate").datepicker().datepicker("setDate", new Date());
    });
   </script> 
   
   <style type="text/css">
   
   td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
   
   
   </style>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">

	<form method="POST" action="uploadRouteCabMapping" enctype="multipart/form-data">
	

<div>

<table>
<p style="font-weight: bold;">Upload Cab Details</p>
<tr>
<td>Cab Numbers</td>
<td><input placeholder="Cab Details File..." id="roasterFile" class= "searchTextBox" maxlength="200" tabindex="1"/>
<input id="exampleInputFile" type="file" name="file" ></td>
</tr>

<td>Date</td>
<td><input class= "searchTextBox routeDate" type="text" id="datepicker" name="routeDate" style="position: relative; z-index: 100000; width: 150px"></td>
</tr>
<tr>
<td>Select Drop Time</td>
<td><select class = "dropDown dropTime" name="startTime" style="width: 250px">
				<option value="05:30 PM">05:30 PM</option>
				<option value="07:30 PM">07:30 PM</option>
			</select></td>
</tr>

<tr><td colspan="2"><button type="submit"
							id="uploadFilesButton" 
							style="width: 150px; height: 35px;" value="Upload">Upload
							Cab Details</button></tr>


</table>
<!-- <div style="" class="accountSeachDiv mainHeading">Upload Cab Numbers</div>
<div style="margin-top:10px;">
<div style="display:inline;">
<input placeholder="Roaster File..." id="roasterFile" class= "searchTextBox" maxlength="200" tabindex="1"/>
<input id="exampleInputFile" type="file" name="file" >
</div>
</div>
<div style="margin-top:10px;"class="accountSeachDiv mainHeading">
<p>Date</p>
<input class= "searchTextBox routeDate" type="text" id="datepicker" name="routeDate" style="position: relative; z-index: 100000; width: 150px">
</div>
<br/>
<div style="margin-top:10px;" class="accountSeachDiv mainHeading">
			<p>Enter Time Slot</p>
			<select class = "dropDown dropTime" name="startTime" style="width: 250px">
				<option value="05:30 PM">05:30 PM</option>
				<option value="07:30 PM">07:30 PM</option>
			</select>
		</div>
<div class="mbr-buttons" style="margin-top: 10px;">
<button type="submit"
							id="uploadFilesButton" 
							style="width: 150px; height: 35px;" value="Upload">Upload
							Cab Details</button>
</div> -->
	
	
	
	
		</form>
	</div>
	<p></p>
	<p></p>
	<div id="jqxgrid">
	
	</div>
	
	</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>

	
	
</body>
</html>