<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>User Details</title>
    <style type="text/css">
    
    .ontop {
				z-index: 999;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				display: none;
				position: absolute;				
				background-color: #cccccc;
				color: #aaaaaa;
				opacity: .96;
				filter: alpha(opacity = 90);
			}
			#popup {
				width: 500px;
				height: 200px;
				position: absolute;
				color: black;
				background-color: white;
				/* To align popup window at the center of screen*/
				top: 50%;
				left: 50%;
				margin-top: -100px;
				margin-left: -150px;
			}
    </style>
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript">
        $(document).ready(function () {
        	
        	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
        	$("input").jqxInput({height: 25, minLength: 1});
            $(".dropDown").jqxDropDownList({autoDropDownHeight: true, width: 150, height: 25 });
            $("input[type='button']").jqxButton({ width: 'auto'});
			
			
			
			$(".routeDate").datepicker().datepicker("setDate", new Date());
			$('.searchBtn').trigger('click');
			
        });
        
    </script>
    
    <style type="text/css">
   
   td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
   
   
   </style>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">

<div class="searchCriteria">

<div id="routeListDiv" style="">
		<p style="font-weight: bold;">User Details</p>
	</div>
<table class = 'searchCriteria'>
		<tr>
			<td style="font-weight: bold;">User Name</td>
			<td>Surjit Singh</td></tr>
			
			<tr><td style="font-weight: bold;">Email</td>
			<td>surjit.singh@domain.com</td></tr>
			<tr><td style="font-weight: bold;">User Type</td>
			<td>Admin</td></tr>
			
		</table>
</div>

<div id="footer">
<%@include file="footer.jsp" %>

</div>
</body>
</html>