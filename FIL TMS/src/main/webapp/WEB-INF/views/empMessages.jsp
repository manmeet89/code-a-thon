<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/css/transport.css"/>" rel="stylesheet">

<title>Route Details</title>
<style type="text/css">

.tableDiv {
    font-family: verdana;
    font-size: -webkit-xxx-large;
    width: 100%;
    text-align: left;
    margin: 0 auto;
    padding: 0;
    top: 0;
    left: 0;
    border-radius: 10px;
}

.messageHeading {
	padding: 10px
}

table {
	border-radius: 10px;
	padding: 10px;
}

</style>
</head>
<body>

<div class="tableDiv">
<c:choose>

	<c:when test="${employeeMessageForm ne null}">
	<div class="messageHeading">
	Cab details are :
	</div>
		<table border="2px" style="width: 98%;height: 98%;position: absolute;" bordercolor="green">
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Date</td>
		<td>${employeeMessageForm.routeDate}</td>
		</tr>
		
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Time Slot</td>
		<td>${employeeMessageForm.startTime}</td>
		</tr>
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Route No</td>
		<td>${employeeMessageForm.routeNo}</td>
		</tr>
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Vendor</td>
		<td>${employeeMessageForm.vendor}</td>
		</tr>
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Cab Number</td>
		<td>${employeeMessageForm.cabNo}</td>
		</tr>
		
		<tr>
		<td style="font-weight: bold;background-color: darkseagreen;">Slot No</td>
		<td>${employeeMessageForm.slotNo}</td>
		</tr>
		<tr><td colspan="2" style="border: solid;">
		<img style="height: 100%; width: 100%;"
				src="<c:url value="/resources/images/Slots.jpg"/>">
		
		</td>
		
		</tr>
		</table>
	</c:when>
	<c:otherwise>
	<b>Roaster not prepared yet!!</b>
	</c:otherwise>

</c:choose>
</div>
<div id="footer">
</body>
</html>