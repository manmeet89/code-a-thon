<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>FIL Transport system</title>
    
    <style type="text/css">
    
    .ontop {
				z-index: 999;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				display: none;
				position: absolute;				
				background-color: #cccccc;
				color: #aaaaaa;
				opacity: .96;
				filter: alpha(opacity = 90);
			}
			#popup {
				width: 500px;
				height: 200px;
				position: absolute;
				color: black;
				background-color: white;
				/* To align popup window at the center of screen*/
				top: 50%;
				left: 50%;
				margin-top: -100px;
				margin-left: -150px;
			}
    </style>
    
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
     <script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
     <script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        	$("input").jqxInput({placeHolder: "Enter...", height: 25, width: 200, minLength: 1});
        	$("input[type='button']").jqxButton({ width: '150'});
        	
        	function reloadGrid() 
        	{
        		var source = {
                datatype: 'json',
                datafields: [{
                    name: 'id',
                    type: 'string'
                }, {
                    name: 'name',
                    type: 'string'
                }, {
                    name: 'regNumber',
                    type: 'string'
                }, {
                    name: 'age',
                    type: 'string'
                }, {
                    name: 'vehicleId',
                    type: 'string'
                }, {
                    name: 'address',
                    type: 'string'
                }, {
                    name: 'mobile',
                    type: 'string'
                }],
                id: 'id',
                url: '${pageContext.request.contextPath}/getDrivers',
                type: 'GET',
                async: true,
                updaterow: function (rowid, rowdata, commit) {
                	
                	commit(true);
                }
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#jqxgrid').jqxGrid({
                theme: 'arctic',
                autoheight: true,
                source: dataAdapter,
                pageable: true,
                sortable: true,
                filterable:true,
                editable: true,
                columnsmenu :false,
                showstatusbar: true,
                width : 800,
                renderstatusbar: function (statusbar) {
                    // appends buttons to the status bar.
                    var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                    var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                    var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                    var saveButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                    container.append(addButton);
                    container.append(deleteButton);
                    container.append(saveButton);
                    statusbar.append(container);
                    addButton.jqxButton({  width: 60, height: 20 });
                    deleteButton.jqxButton({  width: 65, height: 20 });
                    saveButton.jqxButton({  width: 65, height: 20 });
                    // add new row.
                    addButton.click(function (event) {
                  			  pop('popDiv');
                    });
                    // delete selected row.
                    deleteButton.click(function (event) {
                        var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                        var $driverId = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                        $("#jqxgrid").jqxGrid('deleterow', $driverId);
                        $.ajax({
      	        		  type: "POST",
      	        		  url: "${pageContext.request.contextPath}/deleteDriver",
      	        		  data: {
      	        			  driverId : $driverId
      	        		  },
      	        		  success:function(data) {
      	        			reloadGrid();
      	        		    }
      	        		  });
                    });
                    // reload grid data.
                    saveButton.click(function (event) {
                    	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
        	        	var rows = [];
        	        	for (var i = 0; i < griddata.rowscount; i++)
        	        	{
        		        	var myObj = new Object();
        		        	myObj.id = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['id'];
        		        	myObj.name = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['name'];
        		        	myObj.age= $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['age'];
        		        	myObj.address= $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['address'];
        		        	myObj.regNumber = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['regNumber'];
        		        	myObj.mobile = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['mobile'];
        		        	rows.push(myObj);
        	        	}
        	        	
        	        	$.ajax({
        	        		  type: "POST",
        	        		  url: "${pageContext.request.contextPath}/updateDrivers",
        	        		  contentType: "application/json",
        	        		  data: JSON.stringify(rows),
        	        		  success:function(data) {
        	        			  reloadGrid();
        	        		    }
        	        		  });
        	        });
                },
                columns: [{
                    text: 'Name',
                    datafield: 'name',
                    align: 'center',
                    cellsalign: 'center',
                    width : '30%',
                    editable: false
                }, {
                    text: 'Age',
                    datafield: 'age',
                    align: 'center',
                    cellsalign: 'center',
                    width : '10%'
                }, {
                    text: 'Address',
                    datafield: 'address',
                    align: 'center',
                    cellsalign: 'center',
                    width : '20%'
                }, {
                    text: 'Vehicle Number',
                    datafield: 'regNumber',
                    align: 'center',
                    cellsalign: 'center',
                    width : '20%'
                }, {
                    text: 'Mobile Number',
                    datafield: 'mobile',
                    align: 'center',
                    cellsalign: 'center',
                    width : '20%'
                }, {
                    datafield: 'id',
                    align: 'center',
                    cellsalign: 'center',
                    hidden : true
                }, {
                    datafield: 'vehicleId',
                    align: 'center',
                    cellsalign: 'center',
                    hidden : true
                }]
            });
        	}
        	reloadGrid();
            function pop(div) {
				document.getElementById(div).style.display = 'block';
			}
			function hide(div) {
				document.getElementById(div).style.display = 'none';
			}
            
            $('.addVehicleBtn').click(function() {
            	$.ajax({
            		  url: "${pageContext.request.contextPath}/addVehicle",
            		})
            		  .done(function( html ) {
            			  pop('popDiv');
            		  });
            });
            document.onkeydown = function(evt) {
				evt = evt || window.event;
				if (evt.keyCode == 27) {
					hide('popDiv');
				}
			};
			
			$('.addDriverBtnConfirm').click(function() {
				var myObj = new Object();
	        	myObj.name = $('.name').val();
	        	myObj.age = $('.age').val();
	        	myObj.regNumber = $('.regNumber').val();
	        	myObj.address = $('.address').val();
	        	myObj.mobile = $('.mobile').val();
	        	$.ajax({
	        		  type: "POST",
	        		  url: "${pageContext.request.contextPath}/addDriver",
	        		  contentType: "application/json",
					  data : JSON.stringify(myObj),
	        		  success :function(data) {
	        			  hide('popDiv');
	        			  reloadGrid();
	        		  } 
	        		  });
	        });
			
			$('.closeButton').click(function() {
	        	hide('popDiv');
	        });
        });
    </script>
</head>
<body>
<div id="header">
<%@include file="header.jsp" %>
</div>
<div id="content" class="grid">
<div>
<p style="font-weight: bold;">Drivers list</p>
    <div id="jqxgrid"></div>
    <br/>
    <!-- <div id="addVehicleDiv">
    	<input type="button" class="addVehicleBtn" value="Add Vehicle" style="height:30px; width:150px"/>
    	<input type="button" class="refreshBtn" value="Rfresh Data" style="height:30px; width:150px"/>
    </div> -->
     <div id="popDiv" class="ontop" style="display: none">
			<table border="1" id="popup" style="font-size: large;">
			<tr><td colspan="2" style="font-size: x-large; font-weight: bold;">ADD DRIVER FORM
			<div class="closeButton" style="float: right;"><img style="height: 20px; width: 20px;"
				src="resources/images/close_my_icon.jpg"></div>
			</td></tr>
			<tr>
			<td>Name</td>
			<td><input type="text" class="name"></td>
			</tr>
			<tr>
			<td>Age</td>
			<td><input type="text" class="age"></td>
			</tr>
			<tr>
			<td>Address</td>
			<td><input type="text" class="address"></td>
			</tr>
			<tr>
			<td>Vehicle No.</td>
			<td><input type="text" class="regNumber"></td>
			</tr>
			<tr>
			<td>Mobile No.</td>
			<td><input type="text" class="mobile"></td>
			</tr>
			<tr>
			<td colspan="2" align="center"><input type="button" class="addDriverBtnConfirm" value="Add" style="height:30px; width:100px"/></td>
			</tr>
			</table>
		</div>
		
		</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>
		
</body>
</html>