<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>Route Management</title>
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.base.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jqx.arctic.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcore.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdata.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxscrollbar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxmenu.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcheckbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxlistbox.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdropdownlist.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.sort.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.pager.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.selection.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.edit.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxgrid.filter.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxwindow.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxcalendar.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxdatetimeinput.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxbuttons.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqxinput.js" />"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	
    	$( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    	
    	$("input").jqxInput({height: 25 , minLength: 1});
         $(".dropTime").jqxDropDownList({autoDropDownHeight: true, height: 25 });
         $("input[type='button']").jqxButton({ width: 'auto'});
    	$('.getRoutes').click(function() {
    		var date  = $('.routeDate').val();
    		var timeSlot  = $('.dropTime').val();
    		reloadRoutesList(date, timeSlot);
    	});
    	
    	function reloadRoutesList(date, timeSlot) {
    		
    		var slotsSource =
    		{
    			datatype: "json",
    			datafields: [
    				{ name: 'id',type: 'string'},
                    { name: 'name',type: 'string'}
    			],
    			url: '${pageContext.request.contextPath}/getSlots',
    		};
    		 var slotsAdapter = new $.jqx.dataAdapter(slotsSource);
    		
        	var source = {
                    datatype: 'json',
                    datafields: [{
                        name: 'routeId',
                        type: 'string'
                    }, {
                        name: 'transporter',
                        type: 'string'
                    }, {
                        name: 'routeNo',
                        type: 'string'
                    }, {
                        name: 'slotNo',
                        type: 'string'
                    }, {
                        name: 'vehicleNo',
                        type: 'string'
                    },{
                        name: 'slotNo',
                        type: 'string'
                    }],
                    id: 'routeId',
                    url: '${pageContext.request.contextPath}/getRoutes/' + date + '/' + timeSlot,
                    type: 'GET',
                    async: true,
                    updaterow: function (rowid, rowdata, commit) {
                    	commit(true);
                    }
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#jqxgrid').jqxGrid({
                    theme: 'arctic',
                    autoheight: true,
                    source: dataAdapter,
                    pageable: true,
                    sortable: true,
                    filterable:true,
                    columnsmenu :false,
                    editable: true,
                    showstatusbar: true,
                    renderstatusbar: function (statusbar) {
                    	var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
                        // appends buttons to the status bar.
                        var saveChanges = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='images/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Save</span></div>");
                        container.append(saveChanges);
                        statusbar.append(container);
                        saveChanges.jqxButton({  width: 65, height: 20 });
                        // save changes.
                        saveChanges.click(function() {
            	        	var griddata = $('#jqxgrid').jqxGrid('getdatainformation');
            	        	var rows = [];
            	        	for (var i = 0; i < griddata.rowscount; i++)
            	        	{
            		        	var myObj = new Object();
            		        	myObj.routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['routeId'];
            		        	myObj.vehicleNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['vehicleNo'];
            		        	myObj.slotNo = $('#jqxgrid').jqxGrid('getrenderedrowdata', i)['slotNo'];
            		        	rows.push(myObj);
            	        	}
            	        	
            	        	$.ajax({
            	        		  type: "POST",
            	        		  url: "${pageContext.request.contextPath}/assignSlotOrVehicle",
            	        		  contentType: "application/json",
            	        		  data: JSON.stringify(rows),
            	        		  success:function(data) {
            	        			var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
            	                  	var routeId = $('#jqxgrid').jqxGrid('getrenderedrowdata', selectedrowindex)['routeId'];
            	                  	var date  = $('.routeDate').val();
            	            		var timeSlot  = $('.dropTime').val();
            	            		reloadRoutesList(date, timeSlot);
            	        		    }
            	        		  });
            	        });
                    },
                    columns: [{
                        text: 'Route No',
                        datafield: 'routeNo',
                        editable: false,
                        align: 'center',
                        cellsalign: 'center'
                    },{
                        text: 'Transporter',
                        datafield: 'transporter',
                        align: 'center',
                        cellsalign: 'center',
                        editable: false,
                    }, {
                        text: 'Cab No.',
                        datafield: 'vehicleNo',
                        align: 'center',
                        cellsalign: 'center'
                        
                    },  /* {
                        text: 'Slot No.',
                        datafield: 'slotNo',
                        align: 'center',
                        cellsalign: 'center'
                        
                    }  */
                    {
                    	text: 'Slot No', datafield: 'slotNo', displayfield: 'slotNo', columntype: 'dropdownlist',
                    	createeditor: function (row, value, editor) {
                          editor.jqxDropDownList({ source: slotsAdapter, displayMember: 'name', valueMember: 'name', selectedIndex: 0 });
                      }
                	} 
                    , {
                        datafield: 'routeId',
                        cellsformat: 'd', 
                        align: 'center',
                        cellsalign: 'center',
                        hidden : true
                    }]
                });
        }
    	
    	$(".routeDate").datepicker().datepicker("setDate", new Date());
		$('.getRoutes').trigger('click');
    });
</script>

<style type="text/css">
td { 
    padding: 5px;
	}
	
	table { 
    border-spacing: 10px;
    border-collapse: separate;
	}
	
	</style>
</head>
<body>

<div id="header">
<%@include file="header.jsp" %>
</div>

<div id="content" class="grid">

<div class="searchCriteria">

	

		<table class = 'searchCriteria'>
		<tr>
			<td>Route Date</td>
			<td><input type="text" id="datepicker" class = 'routeDate' style="position: relative; z-index: 100000;"></td>
			<td>Select Time Slot</td>
			<td><select class = "dropTime">
				<option value="05:30 PM">05:30 PM</option>
				<option value="07:30 PM">07:30 PM</option>
			</select></td>
			<td colspan="2">
			<input type="button" class="getRoutes" value="Get Routes" style="height:30px; width:100px"/></td>
		</tr>
		</table>
	<p></p>
	<p></p>
	<div id="jqxgrid">
	
	</div>
	
	</div>
</div>
<div id="footer">
<%@include file="footer.jsp" %>

</div>
	
</body>
</html>