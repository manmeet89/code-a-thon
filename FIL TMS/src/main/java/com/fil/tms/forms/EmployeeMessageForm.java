/**
 *
 */
package com.fil.tms.forms;

/**
 * @author ManmeetFIL
 *
 */
public class EmployeeMessageForm {

	String empName;

	String aNumber;

	String routeNo;

	String routeDate;

	String startTime;

	String cabNo;

	String vendor;

	String slotNo;

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName
	 *            the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the aNumber
	 */
	public String getaNumber() {
		return aNumber;
	}

	/**
	 * @param aNumber
	 *            the aNumber to set
	 */
	public void setaNumber(String aNumber) {
		this.aNumber = aNumber;
	}

	/**
	 * @return the routeNo
	 */
	public String getRouteNo() {
		return routeNo;
	}

	/**
	 * @param routeNo
	 *            the routeNo to set
	 */
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	/**
	 * @return the routeDate
	 */
	public String getRouteDate() {
		return routeDate;
	}

	/**
	 * @param routeDate
	 *            the routeDate to set
	 */
	public void setRouteDate(String routeDate) {
		this.routeDate = routeDate;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the cabNo
	 */
	public String getCabNo() {
		return cabNo;
	}

	/**
	 * @param cabNo
	 *            the cabNo to set
	 */
	public void setCabNo(String cabNo) {
		this.cabNo = cabNo;
	}

	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor
	 *            the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the slotNo
	 */
	public String getSlotNo() {
		return slotNo;
	}

	/**
	 * @param slotNo
	 *            the slotNo to set
	 */
	public void setSlotNo(String slotNo) {
		this.slotNo = slotNo;
	}

}
