/**
 *
 */
package com.fil.tms.forms;

/**
 * @author ManmeetFIL
 *
 */
public class SendSMSForm {

	String toNumber;

	String message;

	String cabNo;

	String slotNo;

	String startTime;

	String vendor;

	String routeNo;

	String empName;

	String routeDate;

	/**
	 * @return the toNumber
	 */
	public String getToNumber() {
		return toNumber;
	}

	/**
	 * @param toNumber
	 *            the toNumber to set
	 */
	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the cabNo
	 */
	public String getCabNo() {
		return cabNo;
	}

	/**
	 * @param cabNo
	 *            the cabNo to set
	 */
	public void setCabNo(String cabNo) {
		this.cabNo = cabNo;
	}

	/**
	 * @return the slotNo
	 */
	public String getSlotNo() {
		return slotNo;
	}

	/**
	 * @param slotNo
	 *            the slotNo to set
	 */
	public void setSlotNo(String slotNo) {
		this.slotNo = slotNo;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor
	 *            the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the routeNo
	 */
	public String getRouteNo() {
		return routeNo;
	}

	/**
	 * @param routeNo
	 *            the routeNo to set
	 */
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName
	 *            the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the routeDate
	 */
	public String getRouteDate() {
		return routeDate;
	}

	/**
	 * @param routeDate
	 *            the routeDate to set
	 */
	public void setRouteDate(String routeDate) {
		this.routeDate = routeDate;
	}

}
