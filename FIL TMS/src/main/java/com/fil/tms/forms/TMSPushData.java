/**
 *
 */
package com.fil.tms.forms;

/**
 * @author ManmeetFIL
 *
 */
public class TMSPushData {

	String routeNo;

	String routeDate;

	String startTime;

	String cabNo;

	String vendor;

	String slotNo;

	String mobileNo;

	/**
	 * @return the routeNo
	 */
	public String getRouteNo() {
		return routeNo;
	}

	/**
	 * @param routeNo
	 *            the routeNo to set
	 */
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	/**
	 * @return the routeDate
	 */
	public String getRouteDate() {
		return routeDate;
	}

	/**
	 * @param routeDate
	 *            the routeDate to set
	 */
	public void setRouteDate(String routeDate) {
		this.routeDate = routeDate;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the cabNo
	 */
	public String getCabNo() {
		return cabNo;
	}

	/**
	 * @param cabNo
	 *            the cabNo to set
	 */
	public void setCabNo(String cabNo) {
		this.cabNo = cabNo;
	}

	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor
	 *            the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the slotNo
	 */
	public String getSlotNo() {
		return slotNo;
	}

	/**
	 * @param slotNo
	 *            the slotNo to set
	 */
	public void setSlotNo(String slotNo) {
		this.slotNo = slotNo;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

}
