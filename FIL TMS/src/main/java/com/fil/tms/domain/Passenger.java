/**
 *
 */
package com.fil.tms.domain;

/**
 * @author ManmeetFIL
 *
 */
public class Passenger {

	Long passengerId;

	String dd;

	String serialNo;

	String appFlag;

	String transportProvider;

	String driver;

	String areaCode;

	String corpId;

	String name;

	String address;

	String pickupPoint;

	Long routeId;

	String mobileNo;

	/**
	 * @return the dd
	 */
	public String getDd() {
		return dd;
	}

	/**
	 * @param dd
	 *            the dd to set
	 */
	public void setDd(String dd) {
		this.dd = dd;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo
	 *            the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the appFlag
	 */
	public String getAppFlag() {
		return appFlag;
	}

	/**
	 * @param appFlag
	 *            the appFlag to set
	 */
	public void setAppFlag(String appFlag) {
		this.appFlag = appFlag;
	}

	/**
	 * @return the transportProvider
	 */
	public String getTransportProvider() {
		return transportProvider;
	}

	/**
	 * @param transportProvider
	 *            the transportProvider to set
	 */
	public void setTransportProvider(String transportProvider) {
		this.transportProvider = transportProvider;
	}

	/**
	 * @return the driver
	 */
	public String getDriver() {
		return driver;
	}

	/**
	 * @param driver
	 *            the driver to set
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * @return the areaCode
	 */
	public String getAreaCode() {
		return areaCode;
	}

	/**
	 * @param areaCode
	 *            the areaCode to set
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	/**
	 * @return the corpId
	 */
	public String getCorpId() {
		return corpId;
	}

	/**
	 * @param corpId
	 *            the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the pickupPoint
	 */
	public String getPickupPoint() {
		return pickupPoint;
	}

	/**
	 * @param pickupPoint
	 *            the pickupPoint to set
	 */
	public void setPickupPoint(String pickupPoint) {
		this.pickupPoint = pickupPoint;
	}

	/**
	 * @return the routeId
	 */
	public Long getRouteId() {
		return routeId;
	}

	/**
	 * @param routeId
	 *            the routeId to set
	 */
	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}

	/**
	 * @return the passengerId
	 */
	public Long getPassengerId() {
		return passengerId;
	}

	/**
	 * @param passengerId
	 *            the passengerId to set
	 */
	public void setPassengerId(Long passengerId) {
		this.passengerId = passengerId;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

}
