/**
 *
 */
package com.fil.tms.domain;

/**
 * @author ManmeetFIL
 *
 */
public class Floor {

	String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
