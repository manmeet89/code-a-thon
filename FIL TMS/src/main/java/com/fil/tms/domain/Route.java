/**
 *
 */
package com.fil.tms.domain;

import java.util.Date;

/**
 * @author ManmeetFIL
 *
 */
public class Route {

	Long routeId;

	String htCode;

	String routeNo;

	String cType;

	String transporter;

	String vehicleNo;

	String area;

	String officeLocation;

	String multiplicityFactor;

	String startTime;

	Date routeDate;

	String slotNo;

	/**
	 * @return the htCode
	 */
	public String getHtCode() {
		return htCode;
	}

	/**
	 * @param htCode
	 *            the htCode to set
	 */
	public void setHtCode(String htCode) {
		this.htCode = htCode;
	}

	/**
	 * @return the routeNo
	 */
	public String getRouteNo() {
		return routeNo;
	}

	/**
	 * @param routeNo
	 *            the routeNo to set
	 */
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	/**
	 * @return the cType
	 */
	public String getcType() {
		return cType;
	}

	/**
	 * @param cType
	 *            the cType to set
	 */
	public void setcType(String cType) {
		this.cType = cType;
	}

	/**
	 * @return the transporter
	 */
	public String getTransporter() {
		return transporter;
	}

	/**
	 * @param transporter
	 *            the transporter to set
	 */
	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}

	/**
	 * @return the vehicleNo
	 */
	public String getVehicleNo() {
		return vehicleNo;
	}

	/**
	 * @param vehicleNo
	 *            the vehicleNo to set
	 */
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the officeLocation
	 */
	public String getOfficeLocation() {
		return officeLocation;
	}

	/**
	 * @param officeLocation
	 *            the officeLocation to set
	 */
	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	/**
	 * @return the multiplicityFactor
	 */
	public String getMultiplicityFactor() {
		return multiplicityFactor;
	}

	/**
	 * @param multiplicityFactor
	 *            the multiplicityFactor to set
	 */
	public void setMultiplicityFactor(String multiplicityFactor) {
		this.multiplicityFactor = multiplicityFactor;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the routeId
	 */
	public Long getRouteId() {
		return routeId;
	}

	/**
	 * @param routeId
	 *            the routeId to set
	 */
	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}

	/**
	 * @return the routeDate
	 */
	public Date getRouteDate() {
		return routeDate;
	}

	/**
	 * @param routeDate
	 *            the routeDate to set
	 */
	public void setRouteDate(Date routeDate) {
		this.routeDate = routeDate;
	}

	/**
	 * @return the slotNo
	 */
	public String getSlotNo() {
		return slotNo;
	}

	/**
	 * @param slotNo
	 *            the slotNo to set
	 */
	public void setSlotNo(String slotNo) {
		this.slotNo = slotNo;
	}

}
