package com.fil.tms.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fil.tms.dao.TransportDAO;
import com.fil.tms.domain.Driver;
import com.fil.tms.domain.Floor;
import com.fil.tms.domain.Passenger;
import com.fil.tms.domain.Route;
import com.fil.tms.domain.Slot;
import com.fil.tms.domain.Vehicle;
import com.fil.tms.form.SearchRouteForm;
import com.fil.tms.forms.EmployeeMessageForm;
import com.fil.tms.forms.SendSMSForm;
import com.fil.tms.forms.TMSPushData;
import com.fil.tms.util.PropertiesUtil;
import com.fil.tms.util.TransportUtil;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class TransportController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransportController.class);

	@Autowired
	private TransportDAO transporterDAO;

	@Autowired
	private PropertiesUtil propertyUtil;

	@Autowired
	TransportUtil transportUtil;

	/**
	 * Upload single file using Spring Controller
	 *
	 * @throws IOException
	 */
	@RequestMapping(value = "/uploadRoaster", method = RequestMethod.POST)
	public String uploadRoaster(@RequestParam("file") MultipartFile file, @RequestParam("routeDate") String routeDateStr, ModelMap modelMap)
			throws IOException {

		try {
			int i = 0;

			HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			SimpleDateFormat localDateFormat = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat routeDateFromat = new SimpleDateFormat("dd-MM-yy");
			Date routeDate = routeDateFromat.parse(routeDateStr);
			HSSFRow rowFirst = worksheet.getRow(1);
			String startTime = localDateFormat.format(rowFirst.getCell(8).getDateCellValue());

			transporterDAO.deletePreviousRoutes(routeDate, startTime);

			while (i < worksheet.getLastRowNum()) {
				HSSFRow row = worksheet.getRow(i++);
				if (null != row) {
					if ("Route".equalsIgnoreCase(row.getCell(1).getStringCellValue())) {
						row = worksheet.getRow(i++);
						Route route = new Route();
						route.setHtCode(row.getCell(0).getStringCellValue());
						route.setRouteNo(row.getCell(1).getStringCellValue());
						route.setcType(row.getCell(2).getStringCellValue());
						route.setTransporter(row.getCell(3).getStringCellValue());
						route.setArea(row.getCell(5).getStringCellValue());
						route.setOfficeLocation(row.getCell(6).getStringCellValue());
						route.setMultiplicityFactor(row.getCell(7).getStringCellValue());
						route.setStartTime(localDateFormat.format(row.getCell(8).getDateCellValue()));
						route.setRouteDate(routeDate);
						Long routeId = transporterDAO.addRoute(route);
						i++;
						List<Passenger> passengers = new ArrayList<Passenger>();
						while (true) {
							row = worksheet.getRow(i++);
							if (null == row) {
								break;
							}
							Passenger passenger = new Passenger();
							passenger.setRouteId(routeId);
							passenger.setDd(row.getCell(0).getStringCellValue());
							row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
							passenger.setSerialNo(row.getCell(1).toString());
							row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
							passenger.setAppFlag(row.getCell(2).toString());
							passenger.setTransportProvider(row.getCell(3).getStringCellValue());
							passenger.setDriver(row.getCell(4).getStringCellValue());
							passenger.setAreaCode(row.getCell(5).getStringCellValue());
							passenger.setCorpId(row.getCell(6).getStringCellValue());
							passenger.setName(row.getCell(7).getStringCellValue());
							passenger.setAddress(row.getCell(8).getStringCellValue());
							passenger.setPickupPoint(row.getCell(9).getStringCellValue());
							row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
							passenger.setMobileNo(row.getCell(10).getStringCellValue());
							passengers.add(passenger);
						}
						transporterDAO.addPassengers(passengers);
					}
				}
			}
		} catch (Exception exception) {
			System.out.println(exception.getStackTrace());
			exception.printStackTrace();
			System.out.println("Exception");
		}
		modelMap.put("vendors", transporterDAO.getVendors());
		return "routeList";

	}

	@RequestMapping(value = "/uploadRouteCabMapping", method = RequestMethod.POST)
	public String uploadRouteCabMapping(@RequestParam("file") MultipartFile file, @RequestParam("routeDate") String routeDateStr,
			@RequestParam("startTime") String startTime, ModelMap modelMap) throws IOException {

		try {
			int i = 0;

			HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
			HSSFSheet worksheet = workbook.getSheetAt(0);
			SimpleDateFormat routeDateFromat = new SimpleDateFormat("dd-MM-yy");
			Date routeDate = routeDateFromat.parse(routeDateStr);

			while (i < worksheet.getLastRowNum()) {
				HSSFRow row = worksheet.getRow(++i);
				if (null != row) {
					String routeNo = row.getCell(0).getStringCellValue();
					row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
					String cabNo = row.getCell(1).getStringCellValue();
					transporterDAO.updateCabNoByRoute(cabNo, routeDate, startTime, routeNo);
				}
			}
		} catch (Exception exception) {
			System.out.println(exception.getStackTrace());
			exception.printStackTrace();
			System.out.println("Exception");
		}
		modelMap.put("vendors", transporterDAO.getVendors());
		return "routeList";

	}

	@RequestMapping(value = "/showRoutes", method = RequestMethod.GET)
	public String sowRoutes(ModelMap modelMap) {
		modelMap.put("vendors", transporterDAO.getVendors());
		return "routeList";
	}

	@RequestMapping(value = "/manageVehices", method = RequestMethod.GET)
	public String manageVehices(ModelMap modelMap) {
		modelMap.put("vendors", transporterDAO.getVendors());
		return "manageVehicles";
	}

	@RequestMapping(value = "/manageSlots", method = RequestMethod.GET)
	public String manageSlots() {
		return "manageSlots";
	}

	@RequestMapping(value = "/addVehicle", method = RequestMethod.GET)
	public String addVehicle() {
		return "addVehicle";
	}

	@RequestMapping(value = "/saveVehicle", method = RequestMethod.POST)
	public @ResponseBody String saveVehicle(@RequestParam(value = "vendorId") Long vendorId,
			@RequestParam(value = "regNumber") String regNumber, @RequestParam(value = "model") String model) {
		Vehicle vehicle = new Vehicle();
		vehicle.setModel(model);
		vehicle.setRegNumber(regNumber);
		vehicle.setVendorId(vendorId);
		transporterDAO.saveVehicle(vehicle);
		return "success";
	}

	@RequestMapping(value = "/updateVehicle", method = RequestMethod.POST)
	public @ResponseBody String saveVehicle(@RequestBody List<Vehicle> vehicles) {
		transporterDAO.updateVehicle(vehicles);
		return "success";
	}

	@RequestMapping(value = "/deleteVehicle", method = RequestMethod.POST)
	public @ResponseBody String deleteVehicle(@RequestParam(value = "vehicleId") Long vehicleId) {
		transporterDAO.deleteVehicle(vehicleId);
		return "success";
	}

	@RequestMapping(value = "/getRoutesInfo", method = RequestMethod.POST)
	public @ResponseBody List<Route> getRoutesInfo() throws ParseException {
		return transporterDAO.getRoutes(null);
	}

	@RequestMapping(value = "/searchRoutes", method = RequestMethod.POST)
	public @ResponseBody List<Route> searchRoutes(@RequestParam String date, @RequestParam String dropTime, @RequestParam String vendor)
			throws ParseException {
		SearchRouteForm searchRouteForm = new SearchRouteForm();
		searchRouteForm.setDate(date);
		searchRouteForm.setDropTime(dropTime);
		searchRouteForm.setVendor(vendor);
		return transporterDAO.getRoutes(searchRouteForm);
	}

	@RequestMapping(value = "/getVehicles", method = RequestMethod.POST)
	public @ResponseBody List<Vehicle> getVehicles() {
		return transporterDAO.getVehicles();
	}

	@RequestMapping(value = "/getRoutes/{date}/{time}", method = RequestMethod.GET)
	public @ResponseBody List<Route> getRoutes(@PathVariable("time") String time, @PathVariable("date") String date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");

		return transporterDAO.getRoutesByDateByTime(formatter.parse(date), time);
	}

	@RequestMapping(value = "/assignSlot", method = RequestMethod.GET)
	public String assignSlot() throws ParseException {
		return "assignSlot";
	}

	@RequestMapping(value = "/uploadRoasterHome", method = RequestMethod.GET)
	public String uploadRoasterHome() throws ParseException {
		return "upload";
	}

	@RequestMapping(value = "/uploadCabHome", method = RequestMethod.GET)
	public String uploadCabHome() throws ParseException {
		return "uploadCabDetails";
	}

	@RequestMapping(value = "/assignVehicle/{routeId}/{vehicleNo}/{slotNo}", method = RequestMethod.POST)
	public @ResponseBody String assignVehicle(@PathVariable("routeId") Long routeId, @PathVariable("vehicleNo") String vehicleNo,
			@PathVariable("slotNo") String slotNo) {
		LOGGER.info("**********************Going to assign slot {} to vahicle no. {}", new Object[] { slotNo, vehicleNo });
		transporterDAO.updateVehicleAndSlot(routeId, vehicleNo, slotNo);
		return "success";
	}

	@RequestMapping(value = "/assignVehicleFromApp/{vehicleNo}/{slotNo}/{routeDate}/{startTime}", method = RequestMethod.POST)
	public @ResponseBody String assignVehicleFromApp(@PathVariable("vehicleNo") String vehicleNo, @PathVariable("slotNo") String slotNo,
			@PathVariable("routeDate") String routeDate, @PathVariable("startTime") String startTime) throws ParseException {
		LOGGER.info("**********************Going to assign slot {} to vahicle no. {}", new Object[] { slotNo, vehicleNo });
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
		transporterDAO.updateVehicleAndSlotByApp(vehicleNo, slotNo, formatter.parse(routeDate), startTime);
		sendSMS(vehicleNo, formatter.parse(routeDate), startTime, slotNo);
		return "success";
	}

	private String sendSMS(String vehicleNo, Date routeDate, String startTime, String slotNo) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Route route = transporterDAO.getRouteByVehicle(vehicleNo, routeDate, startTime);
		List<Passenger> passengers = transporterDAO.getPassengersForRoute(route.getRouteId());
		SendSMSForm sendSMSForm = new SendSMSForm();
		sendSMSForm.setCabNo(vehicleNo);
		String floor = transporterDAO.getFloorForSlot(slotNo);
		if (!"".equalsIgnoreCase(floor)) {
			sendSMSForm.setSlotNo(floor + ", " + slotNo);
		} else {
			sendSMSForm.setSlotNo("Not Asssigned");
		}
		sendSMSForm.setStartTime(startTime);
		sendSMSForm.setVendor(route.getTransporter());
		sendSMSForm.setRouteNo(route.getRouteNo());
		sendSMSForm.setRouteDate(formatter.format(route.getRouteDate()));
		List<TMSPushData> pushDatas = new ArrayList<TMSPushData>();
		for (Passenger p : passengers) {
			sendSMSForm.setToNumber(p.getMobileNo());
			sendSMSForm.setEmpName(p.getName());
			transportUtil.sendSMS(sendSMSForm);
			TMSPushData data = new TMSPushData();
			data.setCabNo(vehicleNo);
			data.setMobileNo(p.getMobileNo());
			data.setRouteDate(formatter.format(route.getRouteDate()));
			data.setRouteNo(route.getRouteNo());
			if (!"".equalsIgnoreCase(floor)) {
				data.setSlotNo(floor + ", " + route.getSlotNo());
			} else {
				data.setSlotNo("Not Asssigned");
			}
			data.setStartTime(route.getStartTime());
			data.setVendor(route.getTransporter());
			pushDatas.add(data);
		}
		transportUtil.sendData(pushDatas);
		return "success";
	}

	@RequestMapping(value = "/getMessages/{mobileNo}", method = RequestMethod.GET)
	public String getMessages(@PathVariable("mobileNo") String mobileNo, ModelMap modelMap) throws ParseException {
		EmployeeMessageForm employeeMessageForm = transporterDAO.getEmployeeMessage(mobileNo, getDropTime(), new Date());
		if (null != employeeMessageForm) {
			String floor = transporterDAO.getFloorForSlot(employeeMessageForm.getSlotNo());
			if (!"".equalsIgnoreCase(floor)) {
				employeeMessageForm.setSlotNo(floor + ", " + employeeMessageForm.getSlotNo());
			} else {
				employeeMessageForm.setSlotNo("Not Asssigned");
			}
		}
		modelMap.put("employeeMessageForm", employeeMessageForm);
		return "empMessages";
	}

	@RequestMapping(value = "/getMessagesJSON/{mobileNo}", method = RequestMethod.GET)
	public @ResponseBody EmployeeMessageForm getMessagesJSON(@PathVariable("mobileNo") String mobileNo, ModelMap modelMap)
			throws ParseException {
		EmployeeMessageForm employeeMessageForm = transporterDAO.getEmployeeMessage(mobileNo, getDropTime(), new Date());
		return employeeMessageForm;
	}

	private String getDropTime() {
		Calendar dropTime1 = Calendar.getInstance();
		dropTime1.set(Calendar.MILLISECOND, 0);
		dropTime1.set(Calendar.SECOND, 0);
		dropTime1.set(Calendar.MINUTE, 30);
		dropTime1.set(Calendar.HOUR, 17);

		Calendar dropTime2 = Calendar.getInstance();
		dropTime2.set(Calendar.MILLISECOND, 0);
		dropTime2.set(Calendar.SECOND, 0);
		dropTime2.set(Calendar.MINUTE, 30);
		dropTime2.set(Calendar.HOUR, 19);

		if (Calendar.getInstance().before(dropTime1)) {
			return "05:30 PM";
		} else {
			return "07:30 PM";
		}
	}

	@RequestMapping(value = "/assignVehicleToRoute/{routeId}/{vehicleNo}", method = RequestMethod.POST)
	public @ResponseBody String assignVehicleFromApp(@PathVariable("routeId") Long routeId, @PathVariable("vehicleNo") String vehicleNo)
			throws ParseException {
		transporterDAO.updateVehicleOnRoute(vehicleNo, routeId);
		Route route = transporterDAO.getRouteByRouteId(routeId);
		sendSMS(vehicleNo, route.getRouteDate(), route.getStartTime(), route.getSlotNo());
		return "success";
	}

	@RequestMapping(value = "/assignSlotOrVehicle", method = RequestMethod.POST)
	public @ResponseBody String assignVehicle(@RequestBody List<Route> routes) {
		return transporterDAO.assignSlotOrVehicle(routes);
	}

	@RequestMapping(value = "/getPassengersForRoute", method = RequestMethod.POST)
	public @ResponseBody List<Passenger> getPassengersForRoute(@RequestParam("routeId") Long routeId) {
		return transporterDAO.getPassengersForRoute(routeId);
	}

	@RequestMapping(value = "/addPassengerToRoute", method = RequestMethod.POST)
	public @ResponseBody Long addPassengerToRoute(@RequestBody Passenger passenger) {
		return transporterDAO.addPassengers(Arrays.asList(passenger));
	}

	@RequestMapping(value = "/updateRoutePassengers", method = RequestMethod.POST)
	public @ResponseBody String updateRoutePassengers(@RequestBody List<Passenger> passengers) {
		return transporterDAO.updateRoutePassengers(passengers);
	}

	@RequestMapping(value = "/removePassengerFromRoute", method = RequestMethod.POST)
	public @ResponseBody String removePassengerFromRoute(@RequestParam(value = "passengerId") Long passengerId) {
		transporterDAO.deletePassenger(passengerId);
		return "success";
	}

	@RequestMapping(value = "/getCabAssignedRoutes/{date}/{time}", method = RequestMethod.GET)
	public @ResponseBody List<Route> getCabAssignedRoutes(@PathVariable String date, @PathVariable String time) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
		return transporterDAO.getCabAssignedRoutes(formatter.parse(date), time);
	}

	@RequestMapping(value = "/getCabUnAssignedRoutes/{date}/{time}", method = RequestMethod.GET)
	public @ResponseBody List<Route> getCabUnAssignedRoutes(@PathVariable String date, @PathVariable String time) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
		return transporterDAO.getCabUnAssignedRoutes(formatter.parse(date), time);
	}

	@RequestMapping(value = "/authenticateAppUser/{userName}/{password}", method = RequestMethod.POST)
	public @ResponseBody String authenticateAppUser(@PathVariable String userName, @PathVariable String password) {
		LOGGER.info("****************Login attempt by {} ", new Object[] { userName });
		if (transporterDAO.authenticateUser(userName, TransportUtil.getHashedString(password)) > 0) {
			return "success";
		}
		return "error";
	}

	@RequestMapping(value = "/getSlots", method = RequestMethod.GET)
	public @ResponseBody List<Slot> getSlots() {
		return transporterDAO.getSlots();
	}

	@RequestMapping(value = "/getFloors", method = RequestMethod.GET)
	public @ResponseBody List<Floor> getFloors() {
		List<Floor> floors = new ArrayList<Floor>();
		for (int i = 1; i < 4; i++) {
			Floor floor = new Floor();
			floor.setName("Basement-" + i);
			floors.add(floor);
		}
		return floors;
	}

	@RequestMapping(value = "/updateSlots", method = RequestMethod.POST)
	public @ResponseBody String updateSlots(@RequestBody List<Slot> slots) {
		transporterDAO.updateSlots(slots);
		return "success";
	}

	@RequestMapping(value = "/addSlot", method = RequestMethod.POST)
	public @ResponseBody String addSlot(@RequestBody Slot slots) {
		transporterDAO.insertSlot(slots);
		return "success";
	}

	@RequestMapping(value = "/getDrivers", method = RequestMethod.GET)
	public @ResponseBody List<Driver> getDrivers() {
		return transporterDAO.getDrivers();
	}

	@RequestMapping(value = "/updateDrivers", method = RequestMethod.POST)
	public @ResponseBody String addDrivers(@RequestBody List<Driver> drivers) {
		transporterDAO.updateDrivers(drivers);
		return "success";
	}

	@RequestMapping(value = "/deleteDriver", method = RequestMethod.POST)
	public @ResponseBody String deleteDriver(@RequestParam Long driverId) {
		transporterDAO.deleteDriver(driverId);
		return "success";
	}

	@RequestMapping(value = "/deleteSlot", method = RequestMethod.POST)
	public @ResponseBody String deleteSlot(@RequestParam Long slotId) {
		transporterDAO.deleteSlot(slotId);
		return "success";
	}

	@RequestMapping(value = "/addDriver", method = RequestMethod.POST)
	public @ResponseBody String addDriver(@RequestBody Driver driver) {
		transporterDAO.saveDriver(driver);
		return "success";
	}

	@RequestMapping(value = "/manageDrivers", method = RequestMethod.GET)
	public String manageDrivers() {
		return "manageDrivers";
	}

	@RequestMapping(value = "/assignedRoutes", method = RequestMethod.GET)
	public String assignedRoutes() {
		return "assignedRoutes";
	}

	@RequestMapping(value = "/updateRoutes", method = RequestMethod.POST)
	public String updateRoutes(@RequestBody List<Route> routes) {
		transporterDAO.updateRoutes(routes);
		return "success";
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String homePage() {
		return "routeList";
	}

	@RequestMapping(value = "homePage", method = RequestMethod.GET)
	public String homePageURL() {
		return "home";
	}

	@RequestMapping(value = "userDetails", method = RequestMethod.GET)
	public String userDetails() {
		return "userDetails";
	}
}
