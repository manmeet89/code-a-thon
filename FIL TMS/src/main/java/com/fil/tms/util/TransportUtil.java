/**
 *
 */
package com.fil.tms.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fil.tms.forms.SendSMSForm;
import com.fil.tms.forms.TMSPushData;
import com.google.gson.Gson;

/**
 * @author ManmeetFIL
 *
 */

@Component
public class TransportUtil {

	@Autowired
	PropertiesUtil propertiesUtil;

	public static String getHashedString(String passwordToHash) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passwordToHash.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	public String sendSMS(SendSMSForm sendSMSForm) {
		String user = "username=" + propertiesUtil.getProperty("sms.email");
		String hash = "&hash=" + propertiesUtil.getProperty("sms.hash");

		String message = "&message=Your cab details are: \nDate: " + sendSMSForm.getRouteDate() + "\nTime Slot : " + sendSMSForm
				.getStartTime() + "\nRoute No: " + sendSMSForm.getRouteNo() + "\nVendor: " + sendSMSForm.getVendor() + "\nCab No: "
				+ sendSMSForm.getCabNo() + "\nSlot No: " + sendSMSForm.getSlotNo();
		String sender = "&sender=" + propertiesUtil.getProperty("sms.sender");
		String numbers = "&numbers=" + "91" + sendSMSForm.getToNumber();

		// Send data
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL("http://api.textlocal.in/send/?").openConnection();
			String data = user + hash + numbers + message + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();

			return stringBuffer.toString();
		} catch (Exception e) {
			System.out.println("Error SMS " + e);
			return "Error " + e;
		}
	}

	public String sendData(List<TMSPushData> tmsPushDatas) {
		try {
			URL url = new URL("https://whispering-lowlands-3481.herokuapp.com/addMsgs");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			Gson gson = new Gson();
			String input = gson.toJson(tmsPushDatas);
			System.out.println("JSON to server is :" + input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException exception) {
			exception.printStackTrace();
			return "error";
		} catch (IOException e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}
}
