/**
 *
 */
package com.fil.tms.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author ManmeetFIL
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class PropertiesUtil {

	@Autowired
	private Environment env;

	public String getProperty(String propertyName) {
		return env.getProperty(propertyName);
	}
}
