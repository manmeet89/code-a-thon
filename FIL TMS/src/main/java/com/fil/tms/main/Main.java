/**
 *
 */
package com.fil.tms.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author ManmeetFIL
 *
 */
public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		SimpleDateFormat routeDateFromat = new SimpleDateFormat("dd-MM-yy");
		System.out.println(routeDateFromat.parse("01-10-2015"));

		System.out.println("Hashed Password :: " + getHashedString("test"));

	}

	public static String getHashedString(String passwordToHash) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passwordToHash.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}
}
