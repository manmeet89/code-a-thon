/**
 *
 */
package com.fil.tms.form;

/**
 * @author ManmeetFIL
 *
 */
public class SearchRouteForm {

	String date;

	String dropTime;

	String vendor;

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the dropTime
	 */
	public String getDropTime() {
		return dropTime;
	}

	/**
	 * @param dropTime
	 *            the dropTime to set
	 */
	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}

	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor
	 *            the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
}
