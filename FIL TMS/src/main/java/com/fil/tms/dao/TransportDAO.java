/**
 *
 */
package com.fil.tms.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.fil.tms.domain.Driver;
import com.fil.tms.domain.Passenger;
import com.fil.tms.domain.Route;
import com.fil.tms.domain.Slot;
import com.fil.tms.domain.Vehicle;
import com.fil.tms.domain.Vendor;
import com.fil.tms.form.SearchRouteForm;
import com.fil.tms.forms.EmployeeMessageForm;

/**
 * @author ManmeetFIL
 *
 */
@Repository
public class TransportDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final String INSERT_ROUTE =
			"INSERT INTO ROUTE(HT_CODE, ROUTE_NO, C_TYPE, TRANSPORTER, VEHICLE_NO, AREA, OFFICE_LOCATION, MULTIPLICITY_FACTOR, START_TIME, ROUTE_DATE) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String UPDATE_ROUTE =
			"UPDATE ROUTE SET HT_CODE = ?, ROUTE_NO = ?, C_TYPE = ?, TRANSPORTER = ?, VEHICLE_NO = ?, AREA = ?, OFFICE_LOCATION = ?, MULTIPLICITY_FACTOR = ?, "
					+ "START_TIME = ?, ROUTE_DATE = ? WHERE ROUTE_ID = ?";

	private static final String INSERT_PASSENGER =
			"INSERT INTO PASSENGER(DD, SERIAL_NO, APP_FLAG, TRANSPORT_PROVIDER, DRIVER, AREA_CODE, CORP_ID, NAME, ADDRESS, PICKUP_POINT, ROUTE_ID, MOBILE_NO) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String DELETE_PASSENGER = "DELETE FROM PASSENGER where PASSENGER_ID = ?";

	private static final String UPDATE_PASSENGER =
			"UPDATE PASSENGER SET SERIAL_NO = ?, ADDRESS = ?, PICKUP_POINT = ?, NAME = ?, MOBILE_NO = ? WHERE PASSENGER_ID = ?";

	private static final String SELECT_ROUTES = "SELECT * FROM ROUTE";

	private static final String SEARCH_ROUTES = "SELECT * FROM ROUTE  WHERE ROUTE_DATE = ?";

	private static final String SELECT_VEHICLES =
			"SELECT veh.*, ven.vendor_name FROM VEHICLE veh inner join vendor ven on veh.vendor_id = ven.vendor_id";

	private static final String INSERT_VEHICLE = "INSERT INTO VEHICLE(VENDOR_ID, REGISTRATION_NUMBER, MODEL) VALUES(?, ?, ?)";

	private static final String DELETE_VEHICLE = "DELETE FROM VEHICLE WHERE VEHICLE_ID = ?";

	private static final String UPDATE_VEHICLE = "UPDATE VEHICLE SET REGISTRATION_NUMBER = ?, MODEL = ? WHERE VEHICLE_ID = ?";

	private static final String SELECT_VEHICLE_BY_NUMBER = "select * from vehicle where registration_number = ?";

	private static final String SELECT_VENDORS = "select * from vendor";

	private static final String SELCT_VENDOR_BY_ID = "select * from vendor where vendor_id = ?";

	private static final String INSERT_VENDOR = "INSERT INTO VENDOR(VENDOR_NAME) values(?)";

	private static final String SELECT_ROUTES_BY_DATE = "SELECT * FROM ROUTE WHERE START_TIME = ? AND ROUTE_DATE = ?";

	private static final String UPDATE_VEHICLE_AND_SLOT = "UPDATE ROUTE SET VEHICLE_NO = ?, SLOT_NO = ? WHERE ROUTE_ID = ?";

	public static final String SELECT_MAX_ID = "SELECT MAX(%s) FROM %s";

	public static final String SELECT_ROUTE_PASSENGERS = "SELECT * FROM PASSENGER where route_id = ? order by serial_no";

	public static final String SELECT_CAB_ASSIGNED_ROUTES =
			"select * from route where vehicle_no is not NULL and slot_no is not null and route_date = ? and start_time = ?";

	public static final String SELECT_CAB_NOT_ASSIGNED_ROUTES =
			"select * from route where (vehicle_no is NULL or slot_no is null) and route_date = ? and start_time = ?";

	public static final String AUTHENTICATE_USER = "SELECT count(*) from APP_USER where user_name = ? and password = ?";

	private static final String SELECT_DRIVER = "SELECT d.* from Driver d";

	private static final String INSERT_DRIVER = "INSERT INTO DRIVER(NAME, ADDRESS, AGE, VEHICLE_NUMBER, MOBILE_NO) values (?, ?, ?, ?, ?)";

	private static final String DELETE_DRIVER = "DELETE from Driver where driver_id = ?";

	private static final String UPDATE_DRIVER =
			"UPDATE DRIVER SET NAME = ?, AGE = ?, ADDRESS = ?, VEHICLE_NUMBER = ?, MOBILE_NO = ? WHERE DRIVER_ID = ?";

	public Long authenticateUser(String userName, String password) {
		return jdbcTemplate.queryForObject(AUTHENTICATE_USER, new Object[] { userName, password }, Long.class);
	}

	public List<Route> getCabAssignedRoutes(final Date date, final String time) {
		return jdbcTemplate.query(SELECT_CAB_ASSIGNED_ROUTES, (PreparedStatementSetter) ps -> {
			ps.setDate(1, new java.sql.Date(date.getTime()));
			ps.setString(2, time);

		} , (RowMapper<Route>) (rs, rowNum) -> {
			Route route = new Route();
			route.setRouteId(rs.getLong("ROUTE_ID"));
			route.setArea(rs.getString("AREA"));
			route.setcType(rs.getString("C_TYPE"));
			route.setHtCode(rs.getString("HT_CODE"));
			route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
			route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
			route.setRouteNo(rs.getString("ROUTE_NO"));
			route.setStartTime(rs.getString("START_TIME"));
			route.setTransporter(rs.getString("TRANSPORTER"));
			route.setVehicleNo(rs.getString("VEHICLE_NO"));
			route.setSlotNo(rs.getString("SLOT_NO"));
			return route;
		});
	}

	public List<Route> getCabUnAssignedRoutes(final Date date, final String time) {
		return jdbcTemplate.query(SELECT_CAB_NOT_ASSIGNED_ROUTES, (PreparedStatementSetter) ps -> {
			ps.setDate(1, new java.sql.Date(date.getTime()));
			ps.setString(2, time);

		} , (RowMapper<Route>) (rs, rowNum) -> {
			Route route = new Route();
			route.setRouteId(rs.getLong("ROUTE_ID"));
			route.setArea(rs.getString("AREA"));
			route.setcType(rs.getString("C_TYPE"));
			route.setHtCode(rs.getString("HT_CODE"));
			route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
			route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
			route.setRouteNo(rs.getString("ROUTE_NO"));
			route.setStartTime(rs.getString("START_TIME"));
			route.setTransporter(rs.getString("TRANSPORTER"));
			route.setVehicleNo(rs.getString("VEHICLE_NO"));
			route.setSlotNo(rs.getString("SLOT_NO"));
			return route;
		});
	}

	public List<Passenger> getPassengersForRoute(Long routeId) {
		return jdbcTemplate.query(SELECT_ROUTE_PASSENGERS, new Object[] { routeId }, (rs, rowNum) -> {
			Passenger p = new Passenger();
			p.setDd(rs.getString("dd"));
			p.setSerialNo(rs.getString("serial_no"));
			p.setAppFlag(rs.getString("app_flag"));
			p.setTransportProvider(rs.getString("transport_provider"));
			p.setDriver(rs.getString("driver"));
			p.setAreaCode(rs.getString("area_code"));
			p.setCorpId(rs.getString("corp_id"));
			p.setName(rs.getString("name"));
			p.setAddress(rs.getString("address"));
			p.setPickupPoint(rs.getString("pickup_point"));
			p.setRouteId(rs.getLong("route_id"));
			p.setPassengerId(rs.getLong("passenger_id"));
			p.setMobileNo(rs.getString("MOBILE_NO"));
			return p;
		});
	}

	public Long addRoute(final Route route) {

		jdbcTemplate.update(INSERT_ROUTE, (PreparedStatementSetter) ps -> {
			ps.setString(1, route.getHtCode());
			ps.setString(2, route.getRouteNo());
			ps.setString(3, route.getcType());
			ps.setString(4, route.getTransporter());
			ps.setString(5, route.getVehicleNo());
			ps.setString(6, route.getArea());
			ps.setString(7, route.getOfficeLocation());
			ps.setString(8, route.getMultiplicityFactor());
			ps.setString(9, route.getStartTime());
			ps.setDate(10, new java.sql.Date(route.getRouteDate().getTime()));
		});
		return jdbcTemplate.queryForObject(String.format(SELECT_MAX_ID, new Object[] { "ROUTE_ID", "ROUTE" }), Long.class);
	}

	public Long addPassengers(final List<Passenger> passengers) {

		jdbcTemplate.batchUpdate(INSERT_PASSENGER, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Passenger passenger = passengers.get(i);
				ps.setString(1, passenger.getDd());
				ps.setString(2, passenger.getSerialNo());
				ps.setString(3, passenger.getAppFlag());
				ps.setString(4, passenger.getTransportProvider());
				ps.setString(5, passenger.getDriver());
				ps.setString(6, passenger.getAreaCode());
				ps.setString(7, passenger.getCorpId());
				ps.setString(8, passenger.getName());
				ps.setString(9, passenger.getAddress());
				ps.setString(10, passenger.getPickupPoint());
				ps.setLong(11, passenger.getRouteId());
				ps.setString(12, passenger.getMobileNo());
			}

			@Override
			public int getBatchSize() {
				return passengers.size();
			}
		});
		return jdbcTemplate.queryForObject(String.format(SELECT_MAX_ID, new String[] { "ROUTE_ID", "ROUTE" }), Long.class);
	}

	public void deletePassenger(Long passengerId) {
		jdbcTemplate.update(DELETE_PASSENGER, new Object[] { passengerId });
	}

	public List<Route> getRoutes(final SearchRouteForm searchRouteForm) throws ParseException {
		if (null != searchRouteForm) {
			String dropTime = null;
			String vendor = null;
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
			final java.sql.Date routeDate = new java.sql.Date(formatter.parse(searchRouteForm.getDate()).getTime());
			if (!"All".contentEquals(searchRouteForm.getDropTime())) {
				dropTime = searchRouteForm.getDropTime();
			}
			if (!"All".contentEquals(searchRouteForm.getVendor())) {
				vendor = searchRouteForm.getVendor();
			}
			List<Route> routes = jdbcTemplate.query(SEARCH_ROUTES, (PreparedStatementSetter) ps -> ps.setDate(1, routeDate),
					(RowMapper<Route>) (rs, rowNum) -> {
						Route route = new Route();
						route.setRouteId(rs.getLong("ROUTE_ID"));
						route.setArea(rs.getString("AREA"));
						route.setcType(rs.getString("C_TYPE"));
						route.setHtCode(rs.getString("HT_CODE"));
						route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
						route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
						route.setRouteNo(rs.getString("ROUTE_NO"));
						route.setStartTime(rs.getString("START_TIME"));
						route.setTransporter(rs.getString("TRANSPORTER"));
						route.setVehicleNo(rs.getString("VEHICLE_NO"));
						route.setRouteDate(rs.getDate("route_date"));
						return route;
					});
			Iterator<Route> iterator = routes.iterator();
			while (iterator.hasNext()) {
				boolean removed = false;
				Route route = iterator.next();
				if (null != dropTime && !"ALL".equalsIgnoreCase(dropTime)) {
					if (!route.getStartTime().equalsIgnoreCase(dropTime)) {
						iterator.remove();
						removed = true;
					}
				}
				if (null != vendor && !"ALL".equalsIgnoreCase(vendor)) {
					if (!route.getTransporter().equalsIgnoreCase(vendor) && !removed) {
						iterator.remove();
					}
				}
			}
			return routes;
		}

		return jdbcTemplate.query(SELECT_ROUTES, (RowMapper<Route>) (rs, rowNum) -> {
			Route route = new Route();
			route.setRouteId(rs.getLong("ROUTE_ID"));
			route.setArea(rs.getString("AREA"));
			route.setcType(rs.getString("C_TYPE"));
			route.setHtCode(rs.getString("HT_CODE"));
			route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
			route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
			route.setRouteNo(rs.getString("ROUTE_NO"));
			route.setStartTime(rs.getString("START_TIME"));
			route.setTransporter(rs.getString("TRANSPORTER"));
			route.setVehicleNo(rs.getString("VEHICLE_NO"));
			route.setRouteDate(rs.getDate("route_date"));
			return route;
		});
	}

	public List<Vehicle> getVehicles() {
		return jdbcTemplate.query(SELECT_VEHICLES, (RowMapper<Vehicle>) (rs, rowNum) -> {
			Vehicle vehicle = new Vehicle();
			vehicle.setId(rs.getLong("vehicle_id"));
			vehicle.setModel(rs.getString("model"));
			vehicle.setRegNumber(rs.getString("registration_number"));
			vehicle.setVendorId(rs.getLong("vendor_id"));
			vehicle.setVendorName(rs.getString("vendor_name"));
			return vehicle;
		});
	}

	public List<Vendor> getVendors() {
		return jdbcTemplate.query(SELECT_VENDORS, (RowMapper<Vendor>) (rs, rowNum) -> {
			Vendor vendor = new Vendor();
			vendor.setId(rs.getLong("VENDOR_ID"));
			vendor.setName(rs.getString("VENDOR_NAME"));
			return vendor;
		});
	}

	public int saveVehicle(final Vehicle vehicle) {
		return jdbcTemplate.update(INSERT_VEHICLE, (PreparedStatementSetter) ps -> {
			ps.setLong(1, vehicle.getVendorId());
			ps.setString(2, vehicle.getRegNumber());
			ps.setString(3, vehicle.getModel());
		});
	}

	public int deleteVehicle(final Long vehicleId) {
		return jdbcTemplate.update(DELETE_VEHICLE, (PreparedStatementSetter) ps -> ps.setLong(1, vehicleId));
	}

	public int[] updateVehicle(final List<Vehicle> vehicles) {
		return jdbcTemplate.batchUpdate(UPDATE_VEHICLE, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Vehicle vehicle = vehicles.get(i);
				ps.setString(1, vehicle.getRegNumber());
				ps.setString(2, vehicle.getModel());
				ps.setLong(3, vehicle.getId());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return vehicles.size();
			}
		});
	}

	public List<Route> getRoutesByDateByTime(final Date date, final String time) {
		return jdbcTemplate.query(SELECT_ROUTES_BY_DATE, (PreparedStatementSetter) ps -> {
			ps.setString(1, time);
			ps.setDate(2, new java.sql.Date(date.getTime()));

		} , (RowMapper) (rs, rowNum) -> {
			Route route = new Route();
			route.setRouteId(rs.getLong("ROUTE_ID"));
			route.setRouteNo(rs.getString("ROUTE_NO"));
			route.setVehicleNo(rs.getString("VEHICLE_NO"));
			route.setTransporter(rs.getString("TRANSPORTER"));
			route.setSlotNo(rs.getString("SLOT_NO"));
			return route;
		});
	}

	public String updateVehicleAndSlot(final Long routeId, final String vehicleNo, final String slotNo) {
		jdbcTemplate.update(UPDATE_VEHICLE_AND_SLOT, (PreparedStatementSetter) ps -> {
			ps.setString(1, vehicleNo);
			ps.setString(2, slotNo);
			ps.setLong(3, routeId);

		});
		return "success";
	}

	public String updateRoutePassengers(final List<Passenger> passengers) {
		jdbcTemplate.batchUpdate(UPDATE_PASSENGER, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Passenger p = passengers.get(i);
				ps.setString(1, p.getSerialNo());
				ps.setString(2, p.getAddress());
				ps.setString(3, p.getPickupPoint());
				ps.setString(4, p.getName());
				ps.setString(5, p.getMobileNo());
				ps.setLong(6, p.getPassengerId());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return passengers.size();
			}
		});
		return "success";
	}

	public String assignSlotOrVehicle(final List<Route> routes) {
		jdbcTemplate.batchUpdate(UPDATE_VEHICLE_AND_SLOT, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Route route = routes.get(i);
				ps.setString(1, route.getVehicleNo());
				ps.setString(2, route.getSlotNo());
				ps.setLong(3, route.getRouteId());
			}

			@Override
			public int getBatchSize() {
				return routes.size();
			}
		});
		return "success";
	}

	public List<Driver> getDrivers() {
		return jdbcTemplate.query(SELECT_DRIVER, (RowMapper<Driver>) (rs, rowNum) -> {
			Driver d = new Driver();
			d.setId(rs.getLong("DRIVER_ID"));
			d.setName(rs.getString("NAME"));
			d.setVehicleId(rs.getLong("VEHICLE_ID"));
			d.setRegNumber(rs.getString("vehicle_number"));
			d.setAddress(rs.getString("address"));
			d.setAge(rs.getInt("age"));
			d.setMobile(rs.getString("MOBILE_NO"));
			return d;
		});
	}

	public void saveDriver(final Driver driver) {
		jdbcTemplate.update(INSERT_DRIVER, (PreparedStatementSetter) ps -> {
			ps.setString(1, driver.getName());
			ps.setString(2, driver.getAddress());
			ps.setLong(3, driver.getAge());
			ps.setString(4, driver.getRegNumber());
			ps.setString(5, driver.getMobile());
		});
	}

	public void updateDrivers(final List<Driver> drivers) {
		jdbcTemplate.batchUpdate(UPDATE_DRIVER, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Driver driver = drivers.get(i);
				ps.setString(1, driver.getName());
				ps.setInt(2, driver.getAge());
				ps.setString(3, driver.getAddress());
				ps.setString(4, driver.getRegNumber());
				ps.setString(5, driver.getMobile());
				ps.setLong(6, driver.getId());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return drivers.size();
			}
		});
	}

	public void deleteDriver(final Long driverId) {
		jdbcTemplate.update(DELETE_DRIVER, (PreparedStatementSetter) ps -> {
			ps.setLong(1, driverId);
		});
	}

	public void updateRoutes(final List<Route> routes) {
		jdbcTemplate.batchUpdate(UPDATE_ROUTE, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Route route = routes.get(i);
				ps.setString(1, route.getHtCode());
				ps.setString(2, route.getRouteNo());
				ps.setString(3, route.getcType());
				ps.setString(4, route.getTransporter());
				ps.setString(5, route.getVehicleNo());
				ps.setString(6, route.getArea());
				ps.setString(7, route.getOfficeLocation());
				ps.setString(8, route.getMultiplicityFactor());
				ps.setString(9, route.getStartTime());
				ps.setDate(10, new java.sql.Date(route.getRouteDate().getTime()));
				ps.setLong(11, route.getRouteId());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return routes.size();
			}
		});
	}

	public void deletePreviousRoutes(final Date routeDate, final String startTime) {

		jdbcTemplate.update("DELETE FROM PASSENGER WHERE ROUTE_ID in (SELECT ROUTE_ID FROM ROUTE WHERE route_date = ? and start_time = ?)",
				(PreparedStatementSetter) ps -> {
					ps.setDate(1, new java.sql.Date(routeDate.getTime()));
					ps.setString(2, startTime);
				});
		jdbcTemplate.update("DELETE FROM ROUTE WHERE route_date = ? and start_time = ?", (PreparedStatementSetter) ps -> {
			ps.setDate(1, new java.sql.Date(routeDate.getTime()));
			ps.setString(2, startTime);
		});
	}

	public void updateVehicleAndSlotByApp(final String vehicleNo, final String slotNo, final Date routeDate, final String startTime) {
		jdbcTemplate.update("UPDATE ROUTE SET SLOT_NO = ? WHERE VEHICLE_NO = ? and ROUTE_DATE = ? AND START_TIME = ?",
				(PreparedStatementSetter) ps -> {
					ps.setString(1, slotNo);
					ps.setString(2, vehicleNo);
					ps.setDate(3, new java.sql.Date(routeDate.getTime()));
					ps.setString(4, startTime);

				});
	}

	public void updateVehicleOnRoute(final String vehicleNo, final Long routeId) {
		jdbcTemplate.update("UPDATE ROUTE SET VEHICLE_NO = ? WHERE ROUTE_ID = ?", (PreparedStatementSetter) ps -> {
			ps.setString(1, vehicleNo);
			ps.setLong(2, routeId);
		});
	}

	public void updateCabNoByRoute(final String cabNo, final Date routeDate, final String startTime, final String routeNo) {
		jdbcTemplate.update("UPDATE ROUTE SET VEHICLE_NO = ? WHERE ROUTE_NO = ? and ROUTE_DATE = ? AND START_TIME = ?",
				(PreparedStatementSetter) ps -> {
					ps.setString(1, cabNo);
					ps.setString(2, routeNo);
					ps.setDate(3, new java.sql.Date(routeDate.getTime()));
					ps.setString(4, startTime);

				});
	}

	public Route getRouteByVehicle(final String vehicleNo, final Date routeDate, final String startTime) {

		return jdbcTemplate.queryForObject("SELECT * FROM ROUTE WHERE VEHICLE_NO = ? and ROUTE_DATE = ? and START_TIME = ?",
				(RowMapper<Route>) (rs, rowNum) -> {
					Route route = new Route();
					route.setRouteId(rs.getLong("ROUTE_ID"));
					route.setArea(rs.getString("AREA"));
					route.setcType(rs.getString("C_TYPE"));
					route.setHtCode(rs.getString("HT_CODE"));
					route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
					route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
					route.setRouteNo(rs.getString("ROUTE_NO"));
					route.setStartTime(rs.getString("START_TIME"));
					route.setTransporter(rs.getString("TRANSPORTER"));
					route.setVehicleNo(rs.getString("VEHICLE_NO"));
					route.setRouteDate(rs.getDate("route_date"));
					route.setSlotNo(rs.getString("SLOT_NO"));
					return route;
				} , new Object[] { vehicleNo, new java.sql.Date(routeDate.getTime()), startTime });
	}

	public Route getRouteByRouteId(final Long routeId) {

		return jdbcTemplate.queryForObject("SELECT * FROM ROUTE WHERE ROUTE_ID = ?", (RowMapper<Route>) (rs, rowNum) -> {
			Route route = new Route();
			route.setRouteId(rs.getLong("ROUTE_ID"));
			route.setArea(rs.getString("AREA"));
			route.setcType(rs.getString("C_TYPE"));
			route.setHtCode(rs.getString("HT_CODE"));
			route.setMultiplicityFactor(rs.getString("MULTIPLICITY_FACTOR"));
			route.setOfficeLocation(rs.getString("OFFICE_LOCATION"));
			route.setRouteNo(rs.getString("ROUTE_NO"));
			route.setStartTime(rs.getString("START_TIME"));
			route.setTransporter(rs.getString("TRANSPORTER"));
			route.setVehicleNo(rs.getString("VEHICLE_NO"));
			route.setRouteDate(rs.getDate("route_date"));
			route.setSlotNo(rs.getString("SLOT_NO"));
			return route;
		} , new Object[] { routeId });
	}

	public EmployeeMessageForm getEmployeeMessage(final String mobileNo, String startTime, Date routeDate) {
		return jdbcTemplate.query(
				"SELECT r.*, p.* from ROUTE r inner join Passenger p on p.route_id = r.route_id where p.mobile_no = ? and r.start_time = ? and r.route_date = ?",
				(ResultSetExtractor<EmployeeMessageForm>) rs -> {
					if (!rs.next()) {
						return null;
					}
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
					EmployeeMessageForm employeeMessageForm = new EmployeeMessageForm();
					employeeMessageForm.setaNumber(rs.getString("CORP_ID"));
					employeeMessageForm.setCabNo(rs.getString("VEHICLE_NO"));
					employeeMessageForm.setEmpName(rs.getString("NAME"));
					employeeMessageForm.setRouteNo(rs.getString("ROUTE_NO"));
					employeeMessageForm.setRouteDate(formatter.format(rs.getDate("ROUTE_DATE")));
					employeeMessageForm.setSlotNo(rs.getString("SLOT_NO"));
					employeeMessageForm.setStartTime(rs.getString("START_TIME"));
					employeeMessageForm.setVendor(rs.getString("TRANSPORTER"));
					return employeeMessageForm;
				} , new Object[] { mobileNo, startTime, new java.sql.Date(routeDate.getTime()) });
	}

	public List<Slot> getSlots() {
		return jdbcTemplate.query("SELECT * FROM SLOT", (RowMapper<Slot>) (rs, rowNum) -> {
			Slot slot = new Slot();
			slot.setFloor(rs.getString("FLOOR"));
			slot.setName(rs.getString("SLOT_NAME"));
			slot.setId(rs.getLong("SLOT_ID"));
			return slot;
		});
	}

	public void updateSlots(final List<Slot> slots) {
		jdbcTemplate.batchUpdate("UPDATE SLOT SET SLOT_NAME = ?, FLOOR = ? WHERE SLOT_ID = ?", new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Slot slot = slots.get(i);
				ps.setString(1, slot.getName());
				ps.setString(2, slot.getFloor());
				ps.setLong(3, slot.getId());
			}

			@Override
			public int getBatchSize() {
				return slots.size();
			}
		});
	}

	public void insertSlot(final Slot slot) {
		jdbcTemplate.update("INSERT INTO SLOT(FLOOR, SLOT_NAME) VALUES(?, ?)", (PreparedStatementSetter) ps -> {
			ps.setString(1, slot.getFloor());
			ps.setString(2, slot.getName());
		});
	}

	public void deleteSlot(final Long slotId) {
		jdbcTemplate.update("DELETE FROM SLOT WHERE SLOT_ID = ?", new Object[] { slotId });
	}

	public String getFloorForSlot(final String slot) {
		List<String> floors = jdbcTemplate.query("SELECT FLOOR FROM SLOT WHERE SLOT_NAME = ?", new Object[] { slot }, (RowMapper) (rs,
				rowNum) -> rs.getString("FLOOR"));
		if (floors.size() > 0) {
			return floors.get(0);
		}
		return "";
	}
}
