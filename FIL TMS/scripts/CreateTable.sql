DROP TABLE IF EXISTS Route;
DROP SEQUENCE if exists ROUTE_ID_SEQ;

CREATE SEQUENCE ROUTE_ID_SEQ;
CREATE TABLE Route
(
  HT_CODE character varying,
  ROUTE_NO character varying,
  C_TYPE character varying,
  TRANSPORTER character varying,
  VEHICLE_NO character varying,
  AREA character varying,
  OFFICE_LOCATION character varying,
  MULTIPLICITY_FACTOR character varying,
  START_TIME character varying,
  ROUTE_ID integer DEFAULT NEXTVAL('ROUTE_ID_SEQ') NOT NULL UNIQUE,
  ROUTE_DATE Date DEFAULT now(),
  SLOT_NO character varying
);

DROP TABLE IF EXISTS SLOT;
DROP SEQUENCE if exists SLOT_ID_SEQ;

CREATE SEQUENCE SLOT_ID_SEQ;
create table SLOT
(
	SLOT_ID integer DEFAULT NEXTVAL('ROUTE_ID_SEQ') NOT NULL UNIQUE,
	SLOT_NAME character varying,
	FLOOR character varying
);

DROP TABLE IF EXISTS Passenger;
DROP SEQUENCE if exists PASSENGER_ID_SEQ;

CREATE SEQUENCE PASSENGER_ID_SEQ;
CREATE TABLE Passenger
(
  DD character varying,
  SERIAL_NO character varying,
  APP_FLAG character varying,
  TRANSPORT_PROVIDER character varying,
  DRIVER character varying,
  AREA_CODE character varying,
  CORP_ID character varying,
  NAME character varying,
  ADDRESS character varying,
  PICKUP_POINT character varying,
  route_id integer,
  mobile_no character varying,
  PASSENGER_ID integer DEFAULT NEXTVAL('PASSENGER_ID_SEQ') NOT NULL
);


DROP TABLE IF EXISTS Vendor;
DROP SEQUENCE if exists vendor_ID_SEQ;

CREATE SEQUENCE vendor_ID_SEQ;
CREATE TABLE Vendor
(
  vendor_id integer DEFAULT NEXTVAL('vendor_ID_SEQ') NOT NULL UNIQUE,
  vendor_name character varying
  );

INSERT into Vendor(vendor_name) values('DENEB');
INSERT into Vendor(vendor_name) values('GANGA');
INSERT into Vendor(vendor_name) values('ECO');
INSERT into Vendor(vendor_name) values('GOLDEN');

DROP TABLE IF EXISTS Vehicle;
DROP SEQUENCE if exists vehicle_ID_SEQ;

CREATE SEQUENCE vehicle_ID_SEQ;
CREATE TABLE Vehicle
(
  vehicle_id integer DEFAULT NEXTVAL('vehicle_ID_SEQ') NOT NULL UNIQUE,
  vendor_id integer,
  registration_number character varying,
  model character varying
);



DROP TABLE IF EXISTS app_user;
DROP SEQUENCE if exists app_user_id_seq;

CREATE SEQUENCE app_user_id_seq;
create table app_user(
app_user_id integer NOT NULL DEFAULT nextval('app_user_id_seq'),
user_name character varying,
password  character varying
);

insert into app_user(user_name, password) values('mobileuser', '577da7fee6e9ff57afc5f23882acdf54');

drop table if exists driver;
drop sequence if exists driver_id_seq;

CREATE SEQUENCE driver_id_seq;
create table Driver(
driver_id integer NOT NULL DEFAULT nextval('driver_id_seq'),
name character varying,
address  character varying,
age integer,
vehicle_number  character varying,
vehicle_id integer,
MOBILE_NO character varying
);