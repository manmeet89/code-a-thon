package com.example.robinpatpatia.supervisorapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.JsonReader;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.lang.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.security.AccessController.getContext;


public class MainActivity extends Activity {

    private EditText username;
    private EditText cabNumber;
    private EditText cabNumberOnAssign;
    private EditText DateText;
    private EditText password;

    private Button login;
    private Button scan;
    private Button scanTab;
    private Button assignSlot;
    private Button assignSlotNumber;
    private Button logoutBtn;
    private Button openAssignSlotTab;
    private Button openAssignCabTab;
    private Button logooutOnAssign;


    private static final String MAP_API_URL = "http://fil-tms.herokuapp.com/getRoutes/01-10-2015/05:30%20PM";
    private static final String LOGIN_API_URL = "http://fil-tms.herokuapp.com/authenticateAppUser";
    private static final String ASSIGN_SLOT_API_URL = "http://fil-tms.herokuapp.com/assignVehicleFromApp";
    private static final String SLOT_API_URL = "http://fil-tms.herokuapp.com/getSlots";
    private static final String ROUTE_MAP_API_URL = "http://fil-tms.herokuapp.com/assignVehicleToRoute";
    private Spinner routes;
    private Spinner Slots;
    private Spinner spinnertimeSlot;

    private ImageView img1;

    private Thread t;

    private Date today;

    private String str="";
    private String loginResult="";
    private String assignResult="";
    private String strDate="";
    private String routeJson="";
    private String slotsJson ="";

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupVariables();
    }

    private void setupVariables() {
        username = (EditText) findViewById(R.id.usernameET);
        password = (EditText) findViewById(R.id.passwordET);

        login = (Button) findViewById(R.id.loginBtn);

        today = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        strDate = sdf.format(today);

        addListenerOnButton(login);
    }

    public void authenticateLogin(View view) {
        login.setEnabled(false);
        Log.i("authenticateLogin******", password.getText().toString() + username.getText().toString() + loginResult);
        if(username.getText().toString().length() != 0 || password.getText().toString().length() != 0) {
            if (!username.getText().toString().equalsIgnoreCase("admin")) {
                loginFunction(username.getText().toString(), password.getText().toString());

                while (loginResult.length() == 0) {
                    //-- do nothing
                }
            }
            if (loginResult.equals("success") || (username.getText().toString().equalsIgnoreCase("admin") && password.getText().toString().equalsIgnoreCase("admin"))) {
                loginResult = "";
                openSelectAssign();
            } else if (loginResult.equals("error")) {
                Toast.makeText(getApplicationContext(), "Incorrect User Name or Password!", Toast.LENGTH_SHORT).show();
                loginResult = "";
                login.setEnabled(true);
            } else {
                Toast.makeText(getApplicationContext(), "Other Error!", Toast.LENGTH_SHORT).show();
                loginResult = "";
                login.setEnabled(true);
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Enter Details", Toast.LENGTH_SHORT).show();
            loginResult = "";
            login.setEnabled(true);
        }
    }

    public void addListenerOnLogout(Button logout) {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAction(v);
            }

        });
    }

    public void addListenerOnButton(Button login) {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authenticateLogin(v);
            }

        });
    }

    public void addListenerOnOpenAssignSlotTab(Button ScanTab) {
        ScanTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openScanCabNumber();
            }

        });
    }

    public void addListenerOnAssignSlot(Button AssignSlot) {
        AssignSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSlotAssign();
            }

        });
    }


    public void addListenerAssignSlotNumber(Button AssignSlot) {
        AssignSlot.setEnabled(false);
        AssignSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Slots.getItemAtPosition(Slots.getSelectedItemPosition()).toString() != "-None-" &&
                        cabNumberOnAssign.getText().toString().length() != 0) {
                    assignSlotNumber();
                    Toast.makeText(getApplicationContext(), "Update Sent", Toast.LENGTH_SHORT).show();
                    cabNumberOnAssign.setText("");
                }
                else {
                    Toast.makeText(getApplicationContext(), "Enter Slot and Cab number", Toast.LENGTH_SHORT).show();
                }
            }

        });
        AssignSlot.setEnabled(true);
    }


/*
    //product qr code mode
    public void scanQR(View v) {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 110);
        } catch (ActivityNotFoundException anfe) {
            anfe.printStackTrace();
        }
    }

    //on ActivityResult method
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                //get the extras that are returned from the intent
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
*/

    public void openSelectAssign() {
        //Initializing the Slot Assign Page
        Log.i("openSelectAssign******", strDate);
        setContentView(R.layout.activity_select_to_update);

        openAssignSlotTab  = (Button) findViewById(R.id.Updateslot);
        openAssignCabTab  = (Button) findViewById(R.id.Updatecab);
        logooutOnAssign  = (Button) findViewById(R.id.OnSelectLogout);

        addListenerOnAssignSlot(openAssignSlotTab);
        addListenerOnScan(scan);
    }

    public void addListenerAssignSlot(Button scan) {
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSlotAssign();
            }

        });
    }


    public void openScanCabNumber() {
        getRoutesResponse();

        setContentView(R.layout.activity_supervisor_route_map);
        assignSlot  = (Button) findViewById(R.id.button2);
        scan  = (Button) findViewById(R.id.button);

        addListenerOnAssignSlot(assignSlot);
        addListenerOnScan(scan);

        DateText = (EditText) findViewById(R.id.DateText);
        DateText.setEnabled(false);
        DateText.setText(strDate);

        img1 = (ImageView)findViewById(R.id.camera_image);
        addRoutes();
        getTimeSlotScan();
    }

    public void logoutAction(View v){
        setContentView(R.layout.activity_main);
        setupVariables();
    }


    public void openSlotAssign() {
        //Initializing the Slot List
        getSlotResponse();

        //Initializing the Slot Assign Page
        Log.i("openSlotAssign******", strDate);
        setContentView(R.layout.activity_assign_slot);

        scanTab  = (Button) findViewById(R.id.Next);
        scan  = (Button) findViewById(R.id.button);
        assignSlotNumber  = (Button) findViewById(R.id.AssignSlotNumber);
        logoutBtn = (Button) findViewById(R.id.Logout);

        addListenerOnScan(scan);
        addListenerAssignSlotNumber(assignSlotNumber);
        addListenerOnLogout(logoutBtn);

        DateText = (EditText) findViewById(R.id.DateText);
        DateText.setEnabled(false);
        DateText.setText(strDate);
        cabNumberOnAssign = (EditText) findViewById(R.id.cabNumber);

        getTimeSlotScan();
        addSlot();
    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 12345 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            //img1.setImageBitmap(photo);
            cabNumber = (EditText) findViewById(R.id.cabNumber);
            cabNumber.setText(detectText(photo));
        }
    }*/

    private void getRoutesResponse(){
        String returnStr;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    url = new URL(MAP_API_URL);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    StringBuilder responseStrBuilder = new StringBuilder();

                    String inputStr;
                    while ((inputStr = streamReader.readLine()) != null)
                        responseStrBuilder.append(inputStr);
                    String repStr = responseStrBuilder.toString();
                    routeJson = repStr;

                    urlConnection.disconnect();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }


    private void loginFunction(final String userName, final String password){
        str = "";
        Runnable r = new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    String str = LOGIN_API_URL + "/"+userName+"/"+password;
                    url = new URL(str);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    String repStr =  streamReader.readLine().toString();
                    loginResult = repStr;
                    Log.i("Function****loginResult",loginResult);
                    urlConnection.disconnect();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    private void assignSlotNumber(){

        str = "";
        Runnable r = new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    String str = ASSIGN_SLOT_API_URL + "/" + cabNumberOnAssign.getText() + "/" + Slots.getItemAtPosition(Slots.getSelectedItemPosition()).toString().replace(" ", "%20") +
                            "/" + strDate + "/" + spinnertimeSlot.getItemAtPosition(spinnertimeSlot.getSelectedItemPosition()).toString().replace(" ", "%20");
                    Log.i("assignSlotNumber******", str);
                    url = new URL(str);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    String repStr = streamReader.readLine().toString();
                    assignResult = repStr;
                    Log.i("Func****assignResult", assignResult);
                    urlConnection.disconnect();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    private void mapRoutToCab(){
        str = "";
        Runnable r = new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    Log.i("assignSlotNumber******",assignResult);
                    String str = ROUTE_MAP_API_URL + "/"+"1"+"/"+"5000";
                    url = new URL(str);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    String repStr =  streamReader.readLine().toString();
                    assignResult = repStr;
                    Log.i("Func****assignResult",assignResult);
                    urlConnection.disconnect();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    private void getSlotResponse(){
        str = "";
        Runnable r = new Runnable() {
            @Override
            public void run() {
                URL url = null;
                try {
                    Log.i("assignSlotNumber******",assignResult);
                    String str = SLOT_API_URL;
                    url = new URL(str);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                    String repStr =  streamReader.readLine().toString();
                    slotsJson = repStr;
                    Log.i("Func****assignResult",slotsJson);
                    urlConnection.disconnect();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }


    public void addRoutes() {
        routes = (Spinner) findViewById(R.id.spinner);
        ArrayList list = new ArrayList();
        list.add("-None-");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        while (routeJson == "") {
            //do nothing
        }
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getPickvalues("routeNo"));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        routes.setAdapter(dataAdapter);

    }

    public void addSlot() {
        Slots = (Spinner) findViewById(R.id.SlotNumberSpinner);
        ArrayList list = new ArrayList();
        list.add("-None-");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        while (slotsJson == "") {
            //do nothing
        }
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getPickvalues("name"));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Slots.setAdapter(dataAdapter);

    }

    private ArrayList getPickvalues(String getString) {
        JSONArray arr = null;
        ArrayList list = new ArrayList();
        list.add("-None-");
        try {
            switch (getString){
                case "routeNo":
                    arr = new JSONArray(routeJson);
                    break;
                case "name":
                    arr = new JSONArray(slotsJson);
                    break;
                case "routeId":
                    arr = new JSONArray(routeJson);
                    break;
            }
            Log.i("arr length =  ","length="+arr.length());
            for(int i=0;i<arr.length();i++) {
                JSONObject obj = arr.getJSONObject(i);
                Log.i("obj","obj="+obj.getString(getString));
                list.add(obj.getString(getString));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


   /* public String detectText(Bitmap bitmap) {
        TessDataManager.initTessTrainedData(this.getApplicationContext());
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.setDebug(true);
        String path = "/storage/emulated/0/data/data";

        tessBaseAPI.init(path, "eng"); //Init the Tess with the trained data file, with english language
        //For example if we want to only detect numbers
        //tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "1234567890");
        tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "\\n\\r");
        tessBaseAPI.setImage(bitmap);
        String text = tessBaseAPI.getUTF8Text();

        tessBaseAPI.end();

        return text;
    }*/

    public void getTimeSlotScan() {
        spinnertimeSlot = (Spinner) findViewById(R.id.spinnertimeSlot);
        List<String> list = new ArrayList<String>();
        list.add("05:30 PM");
        list.add("07:30 PM");
        list.add("09:30 PM");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnertimeSlot.setAdapter(dataAdapter);
    }
    private Button scanqr;

    public void scanBar(View v) {
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
        }
    }

    public void scanQR(View v) {
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {

        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                if(contents!= null && contents.trim().length() >4){
                    contents = contents.substring(contents.length()-4,contents.length());
                }
                cabNumberOnAssign.setText(contents);
                Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
}